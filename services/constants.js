const bitcoin = require('bitcoinjs-lib')

const Constants = {
    production: false,
    network: "mainnet",
    enterPercentage: .5,
    statusActivations: false,
    secretKey: "X$dfRb5Uy7$gdE5HrkDa!*lFg+by~RfT",
    secretDeveloperKey: "#?xW47q7-7p=hkzrc&ya#W$#hvM_hjN4uz2LcU_4g+*HA&eLjJV29yD%wxTayxs2",
    packgs : {
        1: {name:"Bronce", price: 250, half: 125, reservPrice: 62.5, halfReservPrice: 31.25, remain: 187.5, halfRemain: 93.75}, 
        2: {name: "Plata", price: 500, half: 250, reservPrice: 125, halfReservPrice: 62.5, remain: 375, halfRemain: 187.5}, 
        3:{name:"Oro", price: 1250, half: 625, reservPrice: 312.5, halfReservPrice: 156.25, remain: 937.5, halfRemain: 468.75}, 
        4:{name:"Diamante", price: 3000, half: 1500, reservPrice: 750, halfReservPrice: 375, remain: 2250, halfRemain: 1125}, 
        5:{name:"Premium", price: 6000, half: 3000, reservPrice: 1500, halfReservPrice: 750, remain: 4500, halfRemain: 2250}, 
        6:{name:"Premium Plus", price: 12000, half:6000, reservPrice: 3000, halfReservPrice: 1500, remain: 9000, halfRemain: 4500}},
    getBlockchainFee : () =>{
        return Constants.production ? 10000 : 600
    },    
    databaseStringConn: ()=>{
        if(Constants.production){
            return 'mongodb://mongoAdmin:Vudh8Li1rzxPxQ7DnkMLS33@127.0.0.1:27017/bitwabi?authSource=admin'
        } else {
            return 'mongodb://localhost:27017/bitwabiprod'
        }
    },
    appUrl: ()=>{
        if(Constants.production){
            return 'https://app.bitwabi.com'
        } else {
            return 'http://localhost:3000'
        }
    },
    btcExplorer: ()=>{
        if(Constants.network === "mainnet"){
            return 'https://blockstream.info/tx'
        } else {
            return 'https://blockstream.info/testnet/tx'
        }
    },
    pckgInfo : (packNumber) =>{
        return Constants.packgs[packNumber]
    },
    tradersIDs:[
        "5f38868a0a71072c2089354e",
        "5f5021b8c5279f3fc41d8685"
    ],
    walletYhon :()=> Constants.network === "mainnet" ? "17QUgAomaF6mttzwowqLi786iZTdwsS9un" : "mnfQSfPHef9E48QtMWRtRhgMQ3KuXs4osg",
    walletAlex :()=> Constants.network === "mainnet" ? "bc1qzqwljxh680rf603fdfgkrjwkltyzexe53w07ld" : "tb1qvjw0tyr6rc7hz400nkx4tl2es3qrqn3v8mfywd",
    walletOscar : ()=>Constants.network === "mainnet" ? "34G5hsmbzWPUavYtjSinDhkMRCs5AmBiPz" : "tb1qthkufzaxdfvymw6hjpd5w5eva5jlqx6ygjma7g",
    walletCristian :()=> Constants.network === "mainnet" ? "3KupJ9c13xngBxmjNLgECNCY7sYt7yuTbj" : "mznGJRvZaDZ3HvDZe1RyExQiNLwUaj6KFT",
    walletDiego :()=> Constants.network === "mainnet" ? "3DS3gaBzSqVRKTUXQHHMfTaKAoTsQMUyUe" : "tb1qnq3z8e5zg9hptq9z8ngdh0eew84jlms5preenm",
    walletCommunitaria : () => Constants.network === "mainnet" ? "38Rk5eGgSDUEQc3acUgwr5URVJmexAuQyP" : "tb1qvhuysvnxmlqh3z8wnecrsngd2wfvnnm97rcf8k",
    globalNetwork :()=> Constants.network === "mainnet" ? bitcoin.networks.bitcoin : bitcoin.networks.testnet,
    blockstream :()=> Constants.network === "mainnet" ? "https://blockstream.info" : "https://blockstream.info/testnet"

};

module.exports = Constants