var mongoose =  require('mongoose');
var child_process = require("child_process");
const Binance = require('binance-api-node').default
const Queue = require("../models/Queue")
const Signal = require("../models/Signal")
const Order = require("../models/Order")
const OrdersQueue = require('../models/OrdersQueue')


const binanceClient = Binance()
let childProceses = {}

class Queues {
    disperseOrder(inPair, outPair, sl, buyPrice, gain, type) {
        return new Promise(async (resolve, reject)=>{
            try {
                let infoExch = await binanceClient.exchangeInfo()
                let symbol = inPair+""+outPair
                let ticker = await binanceClient.dailyStats({ symbol })
                let symbolData = infoExch.symbols.filter(val=> val.symbol === symbol)[0]
                //console.log("el ticker es  ", ticker)
                let impediments = this.verifyFilters(symbolData.filters, buyPrice, ticker.askPrice, sl, gain)
                if (impediments) {
                    throw impediments
                } else {
                    let lotsize = symbolData.filters.filter(val=> val.filterType==="LOT_SIZE")[0]
                    let queue = await Queue.find({})
                    //console.log("la cola es ", queue)
                    let sign = new Signal({
                        kind: "spot", inPair, outPair,
                        entryPrice: buyPrice,
                        stopLoss: sl,
                        takeProfit: gain,type
                    })
                    let signal = await sign.save()

                    //console.log("trae el info exch", symbolData.filters, lotsize)
                    queue.map((val)=>{
                        this.createChild(val, inPair, outPair, sl, buyPrice, gain, type, signal._id, parseFloat(lotsize.stepSize))
                    })
                    resolve()
                }
            } catch (error) {
                console.log("error en dispersar orden: ", error)
                reject(typeof error === "string" ? error : error.message)
            }
        })
    }
    createChild(queueItem, inPair, outPair, sl, buyPrice, gain, type, signalID, lotSize){
        let processID = mongoose.mongo.ObjectID();
        if(queueItem.userID){
            childProceses[queueItem.userID] = {}
            childProceses[queueItem.userID].process = child_process.fork("./orderKeeper/orderKeeper", [JSON.stringify({
                userID: queueItem.userID, inPair, outPair, sl, enterprice: buyPrice, gain, type, signalID, processID, lotSize
            })])
        } else {
            //enviar peticion de que refresque las ordenes abiertas 
            //childProceses[queueItem.userID].process
        }
    }
    verifyFilters(filters, buyPrice, currentPrice, sl, gain){
        if(gain <= buyPrice){
            return "El precio de toma de beneficios debe ser mayor al precio de compra"
        }
        if(sl >= buyPrice){
            return "El precio de pare de perder debe ser menor al precio de compra"
        }
        console.log("en verify el current es ", currentPrice)
        let priceFilter = filters.filter(val=> val.filterType==="PRICE_FILTER")[0]
        let percentPrice = filters.filter(val=> val.filterType==="PERCENT_PRICE")[0]
        if( buyPrice < parseFloat(priceFilter.minPrice) ){
            return "El precio es menor al precio mínimo establecido de compra"
        } 
        if( buyPrice > parseFloat(priceFilter.maxPrice) ){
            return "El precio es mayor al precio máximo establecido de compra"
        }
        let minPercentage = currentPrice * parseFloat(percentPrice.multiplierDown)
        let maxPercentage = currentPrice * parseFloat(percentPrice.multiplierUp)
        if( buyPrice < minPercentage ){
            return "El precio es menor al precio mínimo protegido"
        } 
        if( buyPrice > maxPercentage ){
            return "El precio es mayor al precio máximo protegido"
        }
        return false
    }
    deleteOrder(processID){
        return new Promise((resolve, reject)=>{
            childProceses[processID].send({type: "delete"})
            resolve()
        })
    }
    deleteOrders(){
        return new Promise((resolve, reject)=>{
            Object.keys(childProceses).map(val=>{
                childProceses[val].send({type: "delete"})
            })
            resolve()
        })
    }
    editOCO(sl, gain, processList) {
        return new Promise((resolve, reject)=>{
            console.log("llego con aeeditoco", sl, gain, processList)
            processList.map(processID=>{
                childProceses[processID].send({type: "editoco", sl, tp: gain})
            })
            resolve()
        })
    }
    async myOrdersHistory ( userID ) {
        let orders = await Order.find({ userID })
        return orders
    }
    async myOrdersActive ( userID ) {
        let orders = await OrdersQueue.find({ userID })
        return orders
    }
    editBuyOrders(enterprice){
        return new Promise((resolve, reject)=>{
            Object.keys(childProceses).map(val=>{
                childProceses[val].send({type: "editbuy", enterprice})
            })
            resolve()
        })
    }
}

module.exports = new Queues()