var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var TokenSchema = Schema({
    token: String,
    userID: String,
    address: String,
    quantity: Number,
    selectedPackage: Number,
    refererID: String,
    generatedTime: Date,
    //only for the 50 discount
    halfDiscount: Boolean,
    mobile: String,
    smsCode: String,
    confirmedPhone: Boolean,
    stepPhonePassed: Boolean,
    reservation: Boolean,
    priceBTCinEuros: Number,
    payed: Boolean,
    active: Boolean,
    wif: String,
    txid: String,
    txhash: String,
    retrying: Boolean,
    infoClient : {
        ip: String,
        country_name: String,
        browser: String,
        device: String,
        os: String
    },
    reservationPayment: Number,
}, {timestamps: true});

module.exports = mongoose.model('Token', TokenSchema);