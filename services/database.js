var mysql = require('mysql')

class DataBase {
    constructor(){
        // this.connection = mysql.createConnection({
        //     host: 'localhost',
        //     user: 'escryp_arbitraje',
        //     password: '5v9F;6aEF1d4',
        //     database: 'escryp_arbitraje'
        // })
        this.connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'crypto_dinamicoin'
        })
    }
    getPaymentData(idPayment) {
        return new Promise((resolve, reject)=>{
            this.connection.query('SELECT * FROM payments_view WHERE payments_id='+idPayment, function (err, rows) {
                if (err) reject(err.sqlMessage)
                resolve(rows[0])
            }) 
        })
    }
    updatePayment(idPayment, idTx) {
        return new Promise((resolve, reject)=>{
            let consulta = 'UPDATE payments SET payments_paiddate = CURDATE(), payments_status = 1, payments_ticket = "'+idTx+'" WHERE payments_id ='+idPayment 
            console.log("la consult es ", consulta)
            this.connection.query(consulta, function (err, result) {
                console.log("resp update ", err, result)
                if (err) reject(err.sqlMessage)
                resolve()
            }) 
        })
    }
}

module.exports = new DataBase();