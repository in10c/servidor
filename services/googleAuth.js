const speakeasy = require("speakeasy");
const qrcode = require("qrcode");

module.exports = {
    generateSecretAndQR: async () => {
        const secret = speakeasy.generateSecret({
            name: "Bitwabi",
        });
        return { secret: secret.ascii, key: secret.base32, qrCodeBase64PNG: await qrcode.toDataURL(secret.otpauth_url) };
    },
    verifySecret: (secret, code) => {
        return speakeasy.totp.verify({ secret, encoding: "ascii", token: code });
    },
};
