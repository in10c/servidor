var Payment = require("../models/Payment")
var User = require("../models/User")
const {sendMail, sendActivationMail, sendUpgradeMail} = require("../services/mailer")

class Payments {
    registerNewPayment(userID, description, quantity, quantityBTC, selectedPackage, discount, txid, priceBTCinEuros){
        return new Promise(async (resolve, reject)=>{
            let newPay = new Payment({
                userID, description, quantity, quantityBTC, selectedPackage, discount, priceBTCinEuros, tx : txid
            })
            let user = await User.findById(userID)
            await newPay.save()
            sendMail(user.email, user.name, user.selectedPackage, txid).then(()=>{
                resolve(newPay._id)
            })
        })
    }
    activationPayment(userID, description, quantity, quantityBTC, selectedPackage, discount, txid, priceBTCinEuros){
        return new Promise(async (resolve, reject)=>{
            let newPay = new Payment({
                userID, description, quantity, quantityBTC, selectedPackage, discount, priceBTCinEuros, tx : txid
            })
            let user = await User.findById(userID)
            await newPay.save()
            sendActivationMail(user.email, user.name, user.selectedPackage, txid).then(()=>{
                resolve(newPay._id)
            })
        })
    }
    async getMyPayments(userID){
        let payments = await Payment.find({userID})
        return payments
    }
    async registerNewUpgradePayment(userID, description, quantity, quantityBTC, oldSelectedPackage, selectedPackage, discount, txid, priceBTCinEuros){
            const newPay = new Payment({
                userID, description, quantity, quantityBTC, selectedPackage, discount, priceBTCinEuros, tx : txid
            })
            let user = await User.findById(userID)
            await newPay.save()
            await sendUpgradeMail(user.email, user.name, oldSelectedPackage, selectedPackage, txid)
    }
}

module.exports = new Payments()