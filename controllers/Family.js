var Tree = require("../models/Tree")
var Path = require("../models/Path")
const Tokens = require("./Tokens")

class Family {
    getMyFam(userID){
        return new Promise(async (resolve, reject)=>{
            Path.find({userID}, (err, result)=>{
                if(err || result.length == 0){
                    reject()
                }
                let path = [...result[0].path, userID]
                
                let firstNode = path.shift()
                let firstUser = firstNode ? firstNode : refererID
                console.log("el path es", path)
                Tree.findOne({userID: firstUser}, (err, res)=>{
                    if(err){
                        reject(err)
                    }
                    console.log("trajo el primero ", res)
                    if(path.length == 0 ){
                        resolve(res)
                    } else {
                        let i = this.search(res, path)
                        console.log("trajo el q soy", i)
                        resolve(i)
                    }
                    
                }) 
            })
            
        })
    }
    search(object, path){
        let nowUsed = path.shift()
        let filtered = object.childs.filter(chld => chld.userID == nowUsed )[0]
        if(path.length > 0 ){
            return this.search(filtered, path)
        } else {
            return filtered
        }
    }

    async updateMyFamily(userID, name, lastName, nickName, email, selectedPackage, fullAccount){
        try {
            const halfDiscount = await Tokens.verifyHalfDiscount(userID)
            const { childs } = await this.getTree(userID)
            const treeElement = {userID, name, lastName, nickName, email, selectedPackage, halfDiscount, childs, fullAccount}
            const path = await Path.findOne({userID})
            console.log(path)
            if(path.path.length === 0){
                await Tree.findOneAndUpdate({userID}, treeElement)
            }else{
                const refererID = path.path.pop()
                let referer = await Path.find({userID: refererID})
                let refererPath = JSON.parse(JSON.stringify(referer[0].path))
                console.log("el path del referer", refererPath)
                
                let firstNode = refererPath.shift()
                let query = "childs"
                let counter = 1
                let arrayFilters = []
                while (refererPath.length > 0) {
                    let node = refererPath.shift()
                    let nodeName = "branch"+counter
                    console.log("paso x ref", node, nodeName)
                    counter+= 1
                    query += ".$["+nodeName+"].childs"
                    arrayFilters.push({
                        [nodeName+".userID"] : node
                    })
                }
                if(firstNode){
                    let nodeName = "branch"+counter
                    console.log("paso x ref final", refererID, nodeName)
                    counter+= 1
                    query += ".$["+nodeName+"].childs"
                    arrayFilters.push({
                        [nodeName+".userID"] : refererID
                    })
                }
                query+=".$[branch"+counter+"]"
                arrayFilters.push({
                    ["branch"+counter+".userID"] : userID
                })
                let firstUser = firstNode ? firstNode : refererID
                const treeUpdated = await Tree.update(
                    { userID: firstUser },
                    {
                        "$set": { [query]: treeElement }
                    },
                    {
                        multi: true,
                        arrayFilters
                    }
                )
                console.log(treeUpdated)
            }
            return true
        } catch (error) {
            console.log(error)
            return false
        }
        
    }

    async getTree(userID){
        try {
            const {path} = await Path.findOne({userID})
            if(path.length === 0){
                return await Tree.findOne({userID})
            }else{
                const firstNode = path.shift()
                path.push(userID)
                console.log(path)
                let tree = await Tree.findOne({userID:firstNode})

                for (let i = 0; i < path.length; i++) {
                    const id = path[i];
                    for (let j = 0; j < tree.childs.length; j++) {
                        const child = tree.childs[j];
                        if(child.userID == id){
                            tree = child
                            break
                        }
                    }
                }
                return tree
            }
        } catch (error) {
            console.log(error)
            return false
        }
    }
    async reconstructChilds(family, newReffererID, Users){
        do {
            let person = family.shift()
            console.log("paso por ", person)
            //elimanos path 
            let responseDelete = await Path.deleteOne({userID: person.userID})
            if(responseDelete.deletedCount === 1){
                console.log("se elimino correctamente el path del usuaruo ", person.userID)
                await Users.createPath(person.userID, newReffererID)
                if(person.childs && Array.isArray(person.childs) && person.childs.length > 0){
                    await this.reconstructChilds(person.childs, person.userID, Users)
                }
            } else {
                console.log("NOOO se elimino correctamente el path del usuaruo ", person.userID)
            }
        } while (family.length > 0);
    }
    async deleteFromTree(userID){
        try {
            var {path} = await Path.findOne({ userID })
            if(path.length === 0){
                await Tree.deleteOne({ userID })
                return { success: 1 }
            }else{
                let firstNode = path.shift()
                let query = "childs"
                let counter = 1
                let arrayFilters = []
                //anado los nodos aparte del primero
                while (path.length > 0) {
                    let node = path.shift()
                    let nodeName = "branch"+counter
                    console.log("paso x ref", node, nodeName)
                    counter+= 1
                    query += ".$["+nodeName+"].childs"
                    arrayFilters.push({
                        [nodeName+".userID"] : node
                    })
                }
                let response = await Tree.update( { userID: firstNode }, { $pull:  {[query] : { userID }}  }, { multi: true, arrayFilters } )
                console.log("desde delete from tree", response)
                if(response.nModified === 1){
                    return {success: 1}
                } else {
                    return {success: 0, message: "No se ha eliminado el nodo en profundidad."}
                }
                
            } 
        } catch (error) {
            console.log("se genero error", error)
            return { success: 0, message: error.message ? error.message : error }
        }
        
        
    }
}

module.exports = new Family()