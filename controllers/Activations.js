const Activation = require("../models/Activation")
const User = require("../models/User")
const Constants = require("../services/constants")

class Activations {
    async getactivations () {
        let resp = await Activation.find({})
        return resp
    }
    async getOneActiv (idActiv) {
        let resp = await Activation.findById(idActiv)
        return resp
    }
    verifyActiv (idActiv, currentSelectedPackage) {
        return new Promise((resolve, reject)=>{
            Activation.findById(idActiv, (err, activ)=>{
                if(err){
                    reject(err)
                }
                console.log("la acitvacion es ", activ, currentSelectedPackage)
                let log = ""
                let promises = []
                if(activ.pendingDispersion && Array.isArray(activ.pendingDispersion) && activ.pendingDispersion.length > 0){

                    activ.pendingDispersion.map(val=>{
                        promises.push(User.findById(val.address))
                    })
                    Promise.all(promises).then(clients=>{
                        console.log("LLEGAMOS a todas las resp", clients)
                        clients.map(cli=>{
                            log += "---------  "+cli._id+"  ---------\n"
                            if(!cli.fullAccount){
                                log += "El cliente "+cli.name+" no tiene activa su cuenta\n"
                            }
                            if(!cli.verifiedphone){
                                log += "El cliente "+cli.name+" no ha verificado su correo electrónico\n"
                            }
                            if(!cli.netPaymentsWallet){
                                log += "El cliente "+cli.name+" no ha dado de alta una cartera de beneficios\n"
                            }
                            if( currentSelectedPackage > cli.selectedPackage ){
                                log += "El cliente "+cli.name+" tiene un paquete menor ("+Constants.packgs[cli.selectedPackage].name+") al que se dispersara ("+Constants.packgs[currentSelectedPackage].name+")\n"
                            }
                        })
                        resolve(log)
                    })
                } else {
                    resolve(log)
                }
                
            })
        })
    }

    async preActivVerif(idActiv) {
        try {
            let activ = await Activation.findById(idActiv)
            //console.log("la acitvacion es ", activ, currentSelectedPackage)
            let arrayDisp = [...activ.pendingDispersion]
            if(arrayDisp && Array.isArray(arrayDisp) && arrayDisp.length > 0){
                //console.log("paso porq tengo dispersiones pendientes")
                let dispersionData = await this.getDispersionData(arrayDisp)
                dispersionData.map(obj=>{
                    let cli = obj.usr
                    if(cli.fullAccount && !cli.netPaymentsWallet){
                        throw "El cliente "+cli.name+" tiene activa su cuenta pero no tiene wallet de pagos."
                    }
                })
                return {passed: true, dispersionData}
            } else {
                return {passed: false, error: "No hay nada que dispersar"}
            }
        } catch (error) {
            console.log("se detecto error", error)
            return {passed: false, error}
        }
    }

    async getTransactions(dispersionData, currentSelectedPackage){
        console.log("llego con dispersion data", dispersionData, currentSelectedPackage)
        let company = 0
        let transactions = []
        dispersionData.map(disp=>{
            //si esta activa se le dispersa
            if(disp.usr.active && disp.usr.fullAccount){
                if(currentSelectedPackage > disp.usr.selectedPackage){
                    let minorPrice = Constants.packgs[disp.usr.selectedPackage]
                    let majorPrice = Constants.packgs[currentSelectedPackage]
                    let aprox = disp.quantit * minorPrice / majorPrice
                    transactions.push({ address: disp.usr.netPaymentsWallet, quantity: aprox , usr: disp.usr})
                } else {
                    transactions.push({ address: disp.usr.netPaymentsWallet, quantity: disp.quantity, usr: disp.usr }) 
                }
            //si no esta activa se va a la companbia
            } else {
                company += disp.quantity
            }
        })
        return {company, transactions}
    }

    getDispersionData(arrayDisp) {
        return new Promise((resolve, reject)=>{
            let promises = []

            arrayDisp.map(val=>{
                let prom = new Promise(async (resolveChild, rejectChild)=>{
                    let usr = await User.findById(val.address)
                    resolveChild({...val, usr})
                })
                promises.push(prom)
            })
            Promise.all(promises).then(data=>{
                resolve(data)
            })
        })
    }
    async getUserActivation (userID) {
        let resp = await Activation.findOne({userID})
        return resp
    }
}

module.exports = new Activations()