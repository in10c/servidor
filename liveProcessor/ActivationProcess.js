var mongoose = require('mongoose');
var axios = require("axios").default
const Tokens = require("../controllers/Tokens")
const Dispersion = require("../controllers/Dispersion")
const {address, privateKey, token, topay, userID, topayEuro,priceBTCinEuros, selectedPackage} = JSON.parse(process.argv[2])
const Constants = require("../services/constants")

var today = new Date()
var every = 120000
const constructionTime = today.getTime()
const timeout = 300

function getBalance() {
    console.log("obtendre bvalance de ", address)
    axios.get(Constants.blockstream()+'/api/address/' + address).then((resp)=>{
        let funded = resp.data.chain_stats.funded_txo_sum 
        console.log("la resp es ", resp.data, funded)
        let satoshis = topay * 100000000
        if( funded > 0 && funded >= satoshis){
            Dispersion.disperse(funded, userID, address, privateKey, topayEuro,priceBTCinEuros, selectedPackage)
        } else {    
            repeat(address)
        }
    }, err=>{
        console.log("error", err)
        repeat(address)
    })
}

const getElapsedTime = (time) =>{
    return Math.floor(time / 60000);
}

function repeat(address) {
    let now = new Date().getTime()
    let elapsedTime = now - constructionTime
    let minutes = getElapsedTime(elapsedTime)
    console.log("vamos a repetir", minutes, timeout)
    if(minutes > timeout){
        connectDatabase(()=> {
            Tokens.findAndUpdate(token, {
                payed: false,
                active: false
            })
        })
    } else {
        setTimeout(()=>{
            getBalance()
        }, every)
    }
}

setTimeout(()=>{
    mongoose.Promise = global.Promise;
    // Usamos el método connect para conectarnos a nuestra base de datos
    mongoose.connect(Constants.databaseStringConn(), { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
    .then(() => {
        getBalance()
    })
    .catch(err => console.log(err));
}, every)