var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var TreeSchema = Schema({
    userID: String,
    name: String,
    lastName: String,
    nickName: String,
    email: String,
    selectedPackage: Number,
    fullAccount: Boolean,
    halfDiscount: Boolean,
    childs: Array,
    createdAt: Date,
});

module.exports = mongoose.model('Tree', TreeSchema);