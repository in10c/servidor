const ProtectedBalance = require("../models/ProtectedBalance");
const Apis = require("../models/Api");
const Binance = require("node-binance-api");

class ProtectedBalances {
    async verifyAccesToProtectedBalances(userID) {
        try {
            const api = await Apis.findOne({ userID });
            if (!api || !api.apiKey || !api.apiSecret) {
                throw { message: "El usuario no tiene api" };
            }
            return true;
        } catch (error) {
            return false;
        }
    }

    async createNewProtectedBalance(userID, currency, quantity) {
        try {
            await ProtectedBalance.create({ userID, currency, quantity });
            return true;
        } catch (error) {
            console.log(error.message);
            return false;
        }
    }

    async getProtectedBalance(ID) {
        try {
            const balance = await ProtectedBalance.findById(ID);
            console.log(balance);
            if (!balance) throw { message: "El balance no existe" };
            return balance;
        } catch (error) {
            console.log(error.message);
            return false;
        }
    }

    async getProtectedBalances(userID) {
        try {
            const { apiKey, apiSecret } = await Apis.findOne({ userID });
            const userBinance = new Binance().options({
                APIKEY: apiKey,
                APISECRET: apiSecret,
            });
            const userBalances = await ProtectedBalance.findOne({ userID });
            if (!userBalances) {
                userBalances = await ProtectedBalance.create({ userID, balances: [] });
            }
            let balances = (await userBinance.account()).balances.filter((balance) => balance.free > 0);
            balances = balances.map((balance) => {
                return { isProtected: false, asset: balance.asset, free: balance.free, protectedQuantity: 0 };
            });
            if (userBalances.balances.length > 0) {
                balances = balances.map((balance) => {
                    const coincidenceBalance = userBalances.balances.filter(userBalance => userBalance.asset == balance.asset);
                    if(coincidenceBalance.length > 0){
                        return { isProtected: true, asset: coincidenceBalance[0].asset, free: balance.free, protectedQuantity: coincidenceBalance[0].quantity }
                    }
                    return balance;
                });
            }
            return balances;
        } catch (error) {
            console.log(error.message);
            return false;
        }
    }

    async updateProtectedBalances(userID, balances){
        try {
            await ProtectedBalance.findOneAndUpdate({userID}, {balances});
            return true;
        } catch (error) {
            console.log(error.message);
            return false;
        }
    }

    async updateProtectedBalance(ID, currency, quantity) {
        try {
            const balance = await this.getProtectedBalance(ID);
            if (!balance) return false;
            await ProtectedBalance.findByIdAndUpdate(balance._id, { currency, quantity });
            return true;
        } catch (error) {
            console.log(error.message);
            return false;
        }
    }

    async deleteProtectedBalance(ID) {
        try {
            const balance = await this.getProtectedBalance(ID);
            if (!balance) return false;
            await ProtectedBalance.findByIdAndDelete(balance._id);
            return true;
        } catch (error) {
            console.log(error.message);
            return false;
        }
    }
}

module.exports = new ProtectedBalances();
