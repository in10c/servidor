var User = require("../models/User")
var Tree = require("../models/Tree")
var Path = require("../models/Path")
const Token = require("../models/Token")
const Upgrade = require("../models/Upgrade")
const Tokens = require("../controllers/Tokens")
const Constants = require("../services/constants")
const {fixedToMultiplier} = require("../services/numbers")
const Activation = require("../models/Activation")
const Payments = require("./Payments")
const {getUpgradeBTCAmount} = require("./Bitcoin")
const Family = require('./Family')
const UserSettings = require('../models/UserSettings')
const UserLegal = require('../models/UserLegal')
const { use } = require("../app")

class Users {
    doSignUp(email, name, password, phone, selectedPackage){
        return new Promise(async (resolve, reject)=>{
            let newUser = new User({email, name, password, active: false, phone, selectedPackage})
            await newUser.save()
            resolve(newUser._id)
        })
    }
    async getAll(){
        return await User.find({active: true})
    }
    createPath(userID, refererID){
        return new Promise (async (resolve, reject)=>{
            let user = await User.findById(userID)
            let halfDiscount = await Tokens.verifyHalfDiscount(userID)
            let createdAt = new Date().getTime()
            let treeElement = {email: user.email, name: user.name, selectedPackage: user.selectedPackage, halfDiscount, userID, childs: [], createdAt, fullAccount: user.fullAccount ? user.fullAccount : false}
            console.log("tradigo el user", user)
            if(refererID){
                let referer = await Path.find({userID: refererID})
                let refererPath = JSON.parse(JSON.stringify(referer[0].path))
                console.log("el path del referer", refererPath)

                let newPath = new Path({userID, path: [...refererPath, refererID]})
                await newPath.save()
                let firstNode = refererPath.shift()
                let query = "childs"
                let counter = 1
                let arrayFilters = []
                while (refererPath.length > 0) {
                    let node = refererPath.shift()
                    let nodeName = "branch"+counter
                    console.log("paso x ref", node, nodeName)
                    counter+= 1
                    query += ".$["+nodeName+"].childs"
                    arrayFilters.push({
                        [nodeName+".userID"] : node
                    })
                }
                if(firstNode){
                    let nodeName = "branch"+counter
                    console.log("paso x ref final", refererID, nodeName)
                    counter+= 1
                    query += ".$["+nodeName+"].childs"
                    arrayFilters.push({
                        [nodeName+".userID"] : refererID
                    })
                }
                let firstUser = firstNode ? firstNode : refererID
                Tree.update(
                    { userID: firstUser },
                    {
                        "$push": { [query]: treeElement }
                    },
                    {
                        multi: true,
                        arrayFilters
                    },
                    (err, result)=>{
                        console.log("resp", result, err)
                        resolve()
                    }
                )
                
            } else {
                let newTree = new Tree(treeElement)
                await newTree.save()
                let newPath = new Path({userID, path: []})
                await newPath.save()
                resolve()
            }
        })
        
    }
    login(fields){
        return new Promise((resolve, reject)=>{
            User.find(fields, (err, result)=>{
                if(err || result.length == 0){
                    reject()
                }
                console.log("el result es ", result)
                resolve(result[0])
            })
        })
    }
    notExistPhone(phone){
        return new Promise((resolve, reject)=>{
            User.find({phone, active: true}, (err, result)=>{
                console.log("e", err, result)
                if(err || result.length == 0){
                    resolve()
                }
                reject()
            })
        })
    }
    async notExistNickName(userID, nickName){
        try {
            const users = await User.find({nickName})
            switch (users.length) {
                case 0:
                    return true
                case 1:
                    return users[0]._id == userID
                default:
                    return false
            }
        } catch (error) {
            console.log(error)
            return true
        }
    }
    isNotRegistered(email){
        return new Promise((resolve, reject)=>{
            User.find({email, active: true}, (err, result)=>{
                if(err || result.length == 0){
                    resolve()
                }
                console.log("el result es ", result)
                reject()
            })
        })
    }
    findAndUpdate(userID, data){
        return new Promise((resolve, reject)=>{
            User.findByIdAndUpdate(userID, data, (err,res)=>{
                if(err){
                    console.log("error al traer categorias: ", err)
                    reject()
                }
                resolve()
            })
        })
    }
    getByID(id){
        return new Promise((resolve, reject)=>{
            User.findById(id, (err,res)=>{
                if(err){
                    console.log("error al traer categorias: ", err)
                    reject()
                }
                resolve(res)
            })
        })
    }
    getActivationData(userID){
        return new Promise((resolve, reject)=>{
            Activation.findOne({userID}, (err,res)=>{
                if(err){
                    console.log("error al traer activacion: ", err)
                    reject()
                }
                resolve(res)
            })
        })
    }
    updateActivation(userID, data){
        return new Promise((resolve, reject)=>{
            Activation.findOneAndUpdate({userID}, data, (err,res)=>{
                if(err){
                    console.log("error al traer categorias: ", err)
                    reject()
                }
                resolve()
            })
        })
    }
    getProfile(id){
        return new Promise((resolve, reject)=>{
            User.findById(id, (err,res)=>{
                if(err){
                    console.log("error al traer categorias: ", err)
                    reject()
                }
                Token.findOne({userID: id}, (err,resToken)=>{
                    if(err){
                              console.log("error al traer categorias: ", err)
                        reject()
                    }
                    console.log("de respuestas buenas son", res, resToken)
                    
                    //resToken.halfDiscount
                    //resToken.selectedPackage
                    let result = {
                        halfDiscount: resToken.halfDiscount,
                        selectedPackage: res.selectedPackage,
                        active: res.active,
                        name: res.name,
                        email: res.email,
                        termsAndConditionsAgreement: res.termsAndConditionsAgreement,
                        reservation: res.reservation,
                        reservationPayment: resToken.reservationPayment,
                        remain: resToken.halfDiscount ? Constants.packgs[res.selectedPackage].halfRemain : Constants.packgs[res.selectedPackage].remain,
                        payingTotal: resToken.halfDiscount ? Constants.packgs[res.selectedPackage].half : Constants.packgs[res.selectedPackage].price,
                    }

                    resolve(result)
                })
            })
        })
    }
    async existsUser(userID){
        try {
            const user = await User.findById(userID)
            return !!user
        } catch (error) {
            return false
        }
    }
    async existsEmail(email){
        const user = await User.findOne({email, active: true})
        if(!user){
            return false
        }
        return true
    }
    async existsPhone(phone){
        const user = await User.findOne({phone, active: true})
        if(!user){
            return false
        }
        return true
    }
    async amIdeveloper(userID){
        try {
            const user = await User.findById({_id:userID})
            if(!user || !user.developer){
                return false
            }
            return user.developer
        } catch (error) {
            console.log("error at amIdeveloper")
            return false
        }
    }
    async amItrader(userID){
        try {
            const user = await User.findById({_id:userID})
            if(!user || !user.trader){
                return false
            }
            return user.trader
        } catch (error) {
            console.log("error at amItrader")
            return false
        }
    }
    async changePassword(userID, oldPassword, newPassword){
        console.log("llego con us", userID, oldPassword, newPassword)
        const user = await User.findById(userID)
        console.log("traigo y veo su pas anterior", user)
        if(!user || user.password != oldPassword){
            return false
        }
        console.log("el pass anterior si es el que dice ", user)
        await User.findByIdAndUpdate(userID, {password: newPassword})
        return true
    }
    async restorePassword(email, password){
        //console.log("corriendo nueva restore phone")
        let response = await User.updateMany({email, active: true}, {$set: {password}})
        //console.log("corriendo nueva restore phone", response, password)
        return response.nModified > 0 
    }
    async restorePasswordByPhone(phone, password){
         console.log("toca restaurar pass x telegono", phone, password)
        // const user = await User.findOne({phone, active: true})
        // console.log("respuesyta de users find one x fon y aicit", user)
        // if(!user){
        //     console.log("el usaruo no esta ")
        //     return false
        // }
        // console.log("se cambia contrasena ")
        
        // await User.findOneAndUpdate({phone, active: true}, {password})
        // return true
        //console.log("corriendo nueva restore phone")
        let response = await User.updateMany({phone, active: true}, {$set: {password}})
        console.log("corriendo nueva restore phone", response, response.password, password)
        return response.nModified > 0 
    }
    async changeEmail(userID, email){
        try {
            const user = await User.findById({_id:userID})
            if(!user){
                throw {message: "No existe el usuario"}
            }
            const {name, lastName, nickName, selectedPackage, fullAccount} = await User.findByIdAndUpdate(user._id, {email, verifiedphone: true})
            return {name, lastName, nickName, selectedPackage, fullAccount}
        } catch (error) {
            console.log(error)
            return false
        }
    }
    async setTermsAndConditionsAgreement(userID){
        try {
            const user = await User.findById({_id:userID})
            if(!user){
                throw {message: "No existe el usuario"}
            }
            const userLegals = await UserLegal.findOne({userID})
            if(!userLegals){
                await UserLegal.create({
                    userID, 
                    exonerationResponsabilityAgreement: true, 
                    exonerationResponsabilityAgreementDate: Date.now(),
                    termsAndConditionsAndPrivacyPoliciesAgreement: true, 
                    termsAndConditionsAndPrivacyPoliciesAgreementDate: Date.now(),
                    legalWarningAgreement: true, 
                    legalWarningAgreementDate: Date.now()
                })
            }else{
                await UserLegal.findOneAndUpdate({userID}, {
                    exonerationResponsabilityAgreement: true, 
                    exonerationResponsabilityAgreementDate: Date.now(),
                    termsAndConditionsAndPrivacyPoliciesAgreement: true, 
                    termsAndConditionsAndPrivacyPoliciesAgreementDate: Date.now(),
                    legalWarningAgreement: true, 
                    legalWarningAgreementDate: Date.now()
                })
            }
            return true
        } catch (error) {
            console.log(error)
            return false
        }
    }
    async getTermsAndConditionsAgreement(userID){
        try {
            const userLegals = await UserLegal.findOne({userID})
            if(userLegals && userLegals.exonerationResponsabilityAgreement && userLegals.termsAndConditionsAndPrivacyPoliciesAgreement && userLegals.legalWarningAgreement){
                return true
            }
            return false
        } catch (error) {
            console.log(error)
            return false
        }
    }
    async getuser(userID){
        try {
            const user = await User.findById({_id:userID}, 
                {
                    email: 1,
                    name: 1,
                    lastName: 1,
                    nickName: 1,
                    netPaymentsWallet: 1,
                    selectedPackage: 1, 
                    trader: 1, 
                    developer: 1, 
                    fullAccount: 1, 
                    reservation: 1, 
                    admin: 1,
                    phone: 1,
                    termsAndConditionsAgreement: 1
                })
            if(!user){
                throw {message: "No existe el usuario"}
            }
            user.termsAndConditionsAgreement = await this.getTermsAndConditionsAgreement(userID)
            const userSetting = await UserSettings.findOne({userID})
            const confirmedEmail = userSetting ? userSetting.confirmedEmail : false
            return {...user._doc, ...{confirmedEmail}}
        } catch (error) {
            console.log(error.message)
            return false
        }
    }
    async updateUserInfo(userID, name, lastName, nickName, netPaymentsWallet){
        try {
            const user = await User.findById({_id:userID})
            if(!user){
                throw {message: "No existe el usuario"}
            }
            const {email, selectedPackage, fullAccount} = await User.findByIdAndUpdate(user._id, {name, lastName, nickName, netPaymentsWallet})
            return {email, selectedPackage, fullAccount}
        } catch (error) {
            console.log(error)
            return false
        }
    }
    async upgradeUser(userID, selectedPackage, txid){
        try {
            const user = await User.findById({_id:userID})
            const halfDiscount = await Tokens.verifyHalfDiscount(userID)
            if(!user){
                throw {message: "No existe el usuario"}
            }
            if(selectedPackage <= user.selectedPackage){
                throw {message: "El usuario intento hacer un downgrade de su paquete"}
            }
            const {upgradePaymentBTC, upgradePayment, upgradeDiscount, priceBTCinEuros} = await getUpgradeBTCAmount(selectedPackage, halfDiscount)
            await Payments.registerNewUpgradePayment(userID, 
                `Upgrade de ${Constants.pckgInfo(user.selectedPackage).name} a ${Constants.pckgInfo(selectedPackage).name}`,
                upgradePayment, 
                upgradePaymentBTC, 
                user.selectedPackage,
                selectedPackage,
                upgradeDiscount,
                txid,
                priceBTCinEuros
            )
            const {email, name, lastName, nickName, fullAccount} = await User.findByIdAndUpdate(user._id, {selectedPackage})
            await Upgrade.findOneAndUpdate({userID, active: true}, {
                payed: true,
                txhash: txid
            })
            return {email, name, lastName, nickName, fullAccount}
        } catch (error) {
            console.log(error)
            return false
        }
    }

    async changePath(userID, refererID){

            let user = await User.findById(userID)
            let halfDiscount = await Tokens.verifyHalfDiscount(userID)
            const { childs } = await Family.getTree(userID)
            let createdAt = new Date().getTime()
            let treeElement = {email: user.email, name: user.name, lastName: user.lastName, nickName: user.nickName, selectedPackage: user.selectedPackage, fullAccount: user.fullAccount, halfDiscount, userID, childs, createdAt}
            console.log("tradigo el user", user)

            let referer = await Path.find({userID: refererID})
            let refererPath = JSON.parse(JSON.stringify(referer[0].path))
            console.log("el path del referer", refererPath)

            //Updating user path
            let oldPath = await Path.findOneAndUpdate({userID}, {path: [...refererPath, refererID]})

            //Updating childs paths
            if(childs.length > 0){
                const childsPathToUpdate = await Path.find({"path":{"$elemMatch":{"$eq":userID}}})
                const updatedPaths = childsPathToUpdate.map(
                        (child) => {
                        while(child.path[0] !== userID)
                            child.path.shift()
                        return {
                            userID : child.userID,
                            path: [...refererPath, refererID, ...child.path]
                        }
                    }
                )
                for (const updatedPath of updatedPaths) {
                    await Path.findOneAndUpdate({userID:updatedPath.userID}, {path: updatedPath.path})
                }
            }

            //Adding to new referer
            let firstNode = refererPath.shift()
            let query = "childs"
            let counter = 1
            let arrayFilters = []
            while (refererPath.length > 0) {
                let node = refererPath.shift()
                let nodeName = "branch"+counter
                console.log("paso x ref", node, nodeName)
                counter+= 1
                query += ".$["+nodeName+"].childs"
                arrayFilters.push({
                    [nodeName+".userID"] : node
                })
            }
            if(firstNode){
                let nodeName = "branch"+counter
                console.log("paso x ref final", refererID, nodeName)
                counter+= 1
                query += ".$["+nodeName+"].childs"
                arrayFilters.push({
                    [nodeName+".userID"] : refererID
                })
            }
            let firstUser = firstNode ? firstNode : refererID
            await Tree.update(
                { userID: firstUser },
                {
                    "$push": { [query]: treeElement }
                },
                {
                    multi: true,
                    arrayFilters
                }
            )

            
            //Removing from old referer
            if(oldPath.path.length === 0){
                await Tree.deleteOne({userID});
            }else{
                let oldRefererID = oldPath.path.pop().toString()
                let oldReferer = await Path.find({userID: oldRefererID})
                let oldRefererPath = JSON.parse(JSON.stringify(oldReferer[0].path))
                console.log("el path del referer", oldRefererPath)
                firstNode = oldRefererPath.shift()
                query = "childs"
                counter = 1
                arrayFilters = []
                while (oldRefererPath.length > 0) {
                    let node = oldRefererPath.shift()
                    let nodeName = "branch"+counter
                    console.log("paso x ref", node, nodeName)
                    counter+= 1
                    query += ".$["+nodeName+"].childs"
                    arrayFilters.push({
                        [nodeName+".userID"] : node
                    })
                }
                if(firstNode){
                    let nodeName = "branch"+counter
                    console.log("paso x ref final", oldRefererID, nodeName)
                    counter+= 1
                    query += ".$["+nodeName+"].childs"
                    arrayFilters.push({
                        [nodeName+".userID"] : oldRefererID
                    })
                }
                firstUser = firstNode ? firstNode : oldRefererID
                console.log("firstUser", firstUser)
                console.log("query", query)
                console.log("arrayFilters", arrayFilters)
                await Tree.update(
                    { userID: firstUser },
                    {
                        "$pull": { [query] : { userID } }
                    },
                    {
                        multi: true,
                        arrayFilters
                    }
                )
            }
            
        
    }

    async getNotActivated(){
        let response = await User.find({active: true, reservation:true, selectedPackage: { $gt: 1}})
        return response
    }
}

module.exports = new Users()