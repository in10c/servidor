
var child_process = require("child_process");
const bitcoin = require("bitcoinjs-lib");
const Binance = require('node-binance-api');
var axios = require("axios").default
const ccxt = require ('ccxt');
const Constants = require("../services/constants")
const {sendActivatePaymentMail} = require("../services/mailer")
const networkToUse = Constants.network === "mainnet" ? bitcoin.networks.bitcoin :  bitcoin.networks.testnet
const binance = new Binance()
const blockstream = Constants.network === "mainnet" ? "https://blockstream.info" : "https://blockstream.info/testnet"

const Payments = require("../controllers/Payments")
const Users = require("../controllers/Users")
const Tokens = require("../controllers/Tokens");
const Activation = require("../models/Activation")

const Yhon = Constants.network === "mainnet" ? "17QUgAomaF6mttzwowqLi786iZTdwsS9un" : "mnfQSfPHef9E48QtMWRtRhgMQ3KuXs4osg"
const Alex = Constants.network === "mainnet" ? "bc1qzqwljxh680rf603fdfgkrjwkltyzexe53w07ld" : "tb1qvjw0tyr6rc7hz400nkx4tl2es3qrqn3v8mfywd"
const Oscar = Constants.network === "mainnet" ? "34G5hsmbzWPUavYtjSinDhkMRCs5AmBiPz" : "tb1qthkufzaxdfvymw6hjpd5w5eva5jlqx6ygjma7g"
const Cristian = Constants.network === "mainnet" ? "3KupJ9c13xngBxmjNLgECNCY7sYt7yuTbj" : "mznGJRvZaDZ3HvDZe1RyExQiNLwUaj6KFT"
const Diego = Constants.network === "mainnet" ? "3DS3gaBzSqVRKTUXQHHMfTaKAoTsQMUyUe" : "tb1qnq3z8e5zg9hptq9z8ngdh0eew84jlms5preenm"

const generateAddress = (newToken, selectedPackage, userID, refererID, halfDiscount)=>{
    return new Promise(async (resolve, reject)=>{
        let {reservationPayment, reservationPaymentBTC, priceBTCinEuros} = await getBTCAmount(selectedPackage, halfDiscount)
        
        let {address, wif} = generatePrivate()

        child_process.fork("./liveProcessor/liveProcessor", [JSON.stringify({
            address,
            privateKey: wif,
            token: newToken,
            topay: Constants.production ? reservationPaymentBTC : 0.00022,
            priceBTCinEuros, 
            topayEuro: reservationPayment,
            userID, selectedPackage, refererID
        })])
        resolve({
            wif, address,
            quantity: reservationPaymentBTC,priceBTCinEuros, reservationPayment
        })
    })
}

const generateActivation = (userID, infoClient, profile, newSelectedPackage) => {
    console.log("isers es ", Users)
    return new Promise(async (resolve, reject)=>{
        console.log("isers es ", Users)
        //let profile = await Users.getProfile(userID)
        let today = new Date()
        console.log("la profile me da ", profile)
        let {remainPayment, remainPaymentBTC, priceBTCinEuros} = await getRemainBTCAmount(newSelectedPackage, profile.halfDiscount, profile.reservationPayment)
        let {address, wif} = generatePrivate()
        console.log("desps, ", remainPayment, remainPaymentBTC, priceBTCinEuros, address, wif)
        
        await new Activation({
            userID, address,
            quantity: remainPaymentBTC,
            selectedPackage: newSelectedPackage,
            generatedTime: today, remainPayment,
            halfDiscount: profile.halfDiscount,
            priceBTCinEuros,
            payed: false,
            active: true,
            wif,
            infoClient,
            status: "No dispersada" 
        }).save()

        await sendActivatePaymentMail(profile.email, address, remainPaymentBTC, Constants.appUrl() + "/activateAccount")
        resolve({wif, address, quantity: remainPaymentBTC, priceBTCinEuros, remainPayment})
    })
}

const generatePrivate = () =>{
    const keyPair = bitcoin.ECPair.makeRandom({ network: networkToUse });
    //const keyPair = bitcoin.ECPair.fromWIF("cQwuLmAF5xpvqc9ND7yW13jSQfv9KDzZ4Da15pGau2ZJZLyuzYmQ", TESTNET)
    
    let objpago = bitcoin.payments.p2pkh({
        pubkey: keyPair.publicKey,
        network: networkToUse,
    });
    console.log("obj pago", objpago.address, keyPair.toWIF(),keyPair.publicKey, objpago.output, objpago.output.toString())
    const { address } = objpago

    return {
        address, wif: keyPair.toWIF()
    }
}

const getRemainBTCAmount = (selectedPackage, halfDiscount, reservationPayment)=>{
    return new Promise(async (resolve, reject)=>{
        let priceBTCinEuros = await getBTCPrice()
        let newPackage = Constants.pckgInfo(selectedPackage)
        //quitaremos el descuento
        let takedPrice = halfDiscount ? newPackage.half : newPackage.price
        let remainPayment = takedPrice - parseFloat(reservationPayment)
        //let remainPayment = package.remain
        let remainPaymentBTC = (remainPayment / priceBTCinEuros).toFixed(5)
        resolve({remainPaymentBTC, remainPayment, priceBTCinEuros})
    })
}

const getBTCAmount = (selectedPackage, halfDiscount)=>{
    return new Promise(async (resolve, reject)=>{
        let priceBTCinEuros = await getBTCPrice()
        let package = Constants.pckgInfo(selectedPackage)
        let reservationPayment = halfDiscount ? package.halfReservPrice : package.reservPrice
        let reservationPaymentBTC = (reservationPayment / priceBTCinEuros).toFixed(5)
        resolve({reservationPaymentBTC, reservationPayment, priceBTCinEuros})
    })
}

const getUpgradeBTCAmount = (selectedPackage, halfDiscount)=>{
    return new Promise(async (resolve, reject)=>{
        let priceBTCinEuros = await getBTCPrice()
        let package = Constants.pckgInfo(selectedPackage)
        //quitaremos el descuento
        //let upgradePayment = halfDiscount ? package.price * 0.6 : package.price
        let upgradePayment = package.price
        let upgradeDiscount = package.price - upgradePayment
        let upgradePaymentBTC = (upgradePayment / priceBTCinEuros).toFixed(5)
        resolve({upgradePaymentBTC, upgradePayment, upgradeDiscount, priceBTCinEuros})
    })
}
const getBTCPrice = () =>{
    return new Promise((resolve, reject)=>{
        binance.prices('BTCEUR', (error, ticker) => {
            //console.info("Price of BTC in EURO: ", error, ticker.BTCEUR);
            if(ticker && ticker.BTCEUR){
                resolve(ticker.BTCEUR)
            } else {
                console.log("ha fallado, consultamos con coinbase")
                let coinbase = new ccxt.coinbase()
                coinbase.fetchTicker("BTC/EUR").then(resp=>{
                    console.log("respuesta de coinbase, ", resp.ask)
                    resolve(resp.ask)
                })
            }
        });
    })
}

const getBalance = (address)=> {
    return new Promise((resolve, reject)=>{
        console.log("obtendre bvalance de ", address)
        axios.get(blockstream+'/api/address/' + address).then((resp)=>{
            console.log("la resp es ", resp.data, resp.data.chain_stats.funded_txo_sum)
            resolve(resp.data.chain_stats.funded_txo_sum)
        }, err=>{
            console.log("ocurrio error", err)
            reject()
        })
    })
}
const getBalancePro = (address)=> {
    return new Promise((resolve, reject)=>{
        console.log("obtendre bvalance de ", address)
        axios.get(blockstream+'/api/address/' + address).then((resp)=>{
            console.log("la resp es ", resp.data, resp.data.chain_stats.funded_txo_sum)
            resolve({
                balance: resp.data.chain_stats.funded_txo_sum,
                mempool : resp.data.mempool_stats.funded_txo_sum
            })
        }, err=>{
            console.log("ocurrio error", err)
            reject()
        })
    })
}
const getInputs = (hash) => {
    return new Promise((resolve, reject)=>{
        axios.get(blockstream+"/api/tx/"+hash+"/hex").then(urlTxes=>{
            urlTxes = urlTxes.data
            console.log("se pondra hex", urlTxes)
            resolve(Buffer.from(urlTxes, 'hex'))
        }) 
    })
}
const manualPaymentProcess = (tkn) =>{
    return new Promise((resolve, reject)=>{
        this.getBalance(tkn.address).then((balance)=>{
            console.log("trajo de balance", balance, blockstream+'/api/address/' + tkn.address+"/txs")
            axios.get(blockstream+'/api/address/' + tkn.address+"/txs").then((respTXS)=>{
                //genero key mediante wif
                const keyPair = bitcoin.ECPair.fromWIF(tkn.wif, networkToUse)
                let inputTX = respTXS.data[0]
                console.log("el poutput es ", inputTX.vout)
                let voutN;
                inputTX.vout.map((val, indx)=>{
                    console.log("paso por vou", val, indx, tkn.address, val.scriptpubkey_address)
                    if(val.scriptpubkey_address === tkn.address){
                        voutN = indx
                    }
                })
                console.log("el val elegido es  ", voutN)
               
                let blockchainFee = 10000
                let todisperse = balance - blockchainFee
                let pctgBig = (todisperse * .24).toFixed(0)
                let pctgMin = (todisperse * .04).toFixed(0)
                console.log("revision", blockchainFee, todisperse, pctgBig, pctgMin)
                getInputs(inputTX.txid).then((nonWitnessUtxo)=>{
                    //creo transaccion
                    console.log("entro, ", {
                        address: Cristian,
                        value: pctgBig,
                    }, Cristian, pctgMin, pctgBig, Yhon, Oscar, Alex, Diego)
                    const psbt = new bitcoin.Psbt({network: networkToUse})
                    .addInput({
                        hash: inputTX.txid,
                        index: voutN,
                        nonWitnessUtxo,
                    }) 
                    .addOutput({
                        address: Cristian,
                        value: parseInt(pctgBig),
                    }) 
                    .addOutput({
                        address: Yhon,
                        value: parseInt(pctgBig),
                    })
                    .addOutput({
                        address: Oscar,
                        value: parseInt(pctgBig),
                    })
                    .addOutput({
                        address: Alex,
                        value: parseInt(pctgBig),
                    })
                    .addOutput({
                        address: Diego,
                        value: parseInt(pctgMin),
                    })
                    //firmo transaccion
                    psbt.signInput(0, keyPair)
                    psbt.validateSignaturesOfInput(0, keyPair.publicKey)
                    psbt.finalizeAllInputs()
                    console.log('Transaction hexadecimal:')
                    console.log(psbt.extractTransaction().toHex())
                    resolve(psbt.extractTransaction().toHex())
                })
            }, err=>{
                console.log("error", err)
                repeat(address)
            })
        }, err=>{
            console.log("no se pugo traer balance")
        })
        
    })
}

const broadcast = (txHex, tkn) => {
    return new Promise((resolve, reject)=>{
        axios.post(blockstream+'/api/tx', txHex).then( async resp => {
            console.log("transacction submited", resp, resp.data)
            let txid = resp.data
            let userID = tkn.userID
            await Payments.registerNewPayment(userID, "Reservación de licencia", tkn.reservationPayment, tkn.quantity, tkn.selectedPackage, 0, txid, tkn.priceBTCinEuros)
            await Users.findAndUpdate(userID, {
                active:true,
                reservation: true
            })
            await Users.createPath(userID, tkn.refererID)
            await Tokens.findAndUpdate(tkn.token, {
                payed: true,
                active: false,
                txid,
                txhash: txHex
            })
            resolve()
        }).catch( err => {
            console.log("hubo un error", err)
            reject(txHex)
        })
    })
}

const generateUser = async (tkn) => {
    let userID = tkn.userID
    await Payments.registerNewPayment(userID, "Reservación de licencia", tkn.reservationPayment, tkn.quantity, tkn.selectedPackage, 0, "TRANSACCION NO DETECTADA - REGISTRO MANUAL", tkn.priceBTCinEuros)
    await Tokens.findAndUpdate(tkn.token, {
        payed: true,
        active: false,
        txhash: "TRANSACCION NO DETECTADA - REGISTRO MANUAL"
    })
}

const sendCommissions = async (company, transactions, wif, address) => {
    //genero key mediante wif
    const keyPair = bitcoin.ECPair.fromWIF(wif, networkToUse)
    //remove blockchain fee
    company -= Constants.getBlockchainFee()
    //instatiate psbt
    const psbt = new bitcoin.Psbt({network: networkToUse})
    //get all inputs, add and sign
    let txInputs = await getAddressInputs(address)
    let counterInput = 0
    for (let index = 0; index < txInputs.length; index++) {
        psbt.addInput(txInputs[index])
        counterInput += 1
    }
    
    //add commission outputs
    for (let oIndx = 0; oIndx < transactions.length; oIndx++) {
        let commissionTX = transactions[oIndx];
        psbt.addOutput({
            address: commissionTX.address,
            value: parseInt(commissionTX.quantity),
        }) 
    }
    //add company tx out
    psbt.addOutput({
        address: Constants.walletCommunitaria(),
        value: company,
    })
    for (let indxInpt = 0; indxInpt < counterInput; indxInpt++) {
        //tx sign
        psbt.signInput(indxInpt, keyPair)
        psbt.validateSignaturesOfInput(indxInpt, keyPair.publicKey)
    }
    psbt.finalizeAllInputs()
    let hex = psbt.extractTransaction().toHex()
    try {
        let resp = await axios.post(blockstream+'/api/tx', hex)
        return {success: 1, txid: resp.data, transactions}  
    } catch (error) {
        console.log("ocurrio un error en broad", error)
        return {success: 0, error}  
    }
}

const getAddressInputs = (address) => {
    return new Promise((resolve, reject)=>{
        axios.get(blockstream+'/api/address/' + address+"/utxo").then(respTXS=>{
            let promises = []
            console.log(" antes de inputs", address, respTXS.data)
            respTXS.data.map(inputTX=>{
                let prom = new Promise((resolveChild, rejectChild)=>{
                    getInputs(inputTX.txid).then(nonWitnessUtxo=>{
                        resolveChild({
                            hash: inputTX.txid,
                            index: inputTX.vout,
                            nonWitnessUtxo,
                        }) 
                    })
                })
                promises.push(prom)
            })
            Promise.all(promises).then(data=>{
                console.log("la data de todas las promesas", data)
                resolve(data)
            })
        })
    })
}

exports.generateUser = generateUser
exports.generateAddress = generateAddress
exports.manualPaymentProcess = manualPaymentProcess
exports.broadcast = broadcast
exports.getBalance = getBalance
exports.generateActivation = generateActivation
exports.getBTCAmount = getBTCAmount
exports.getUpgradeBTCAmount = getUpgradeBTCAmount
exports.getBalancePro = getBalancePro
exports.sendCommissions = sendCommissions
exports.getAddressInputs = getAddressInputs
exports.getBTCPrice = getBTCPrice
exports.generatePrivate = generatePrivate