const Queue = require("../models/Queue")
const Apis = require("../models/Api")
const Signal = require("../models/Signal")
const OrdersQueue = require("../models/OrdersQueue")
const Metrics = require("../models/Metrics")
const Users = require("../controllers/Users")

class Queues {
    putInQueue(){
        return new Promise((resolve, reject)=>{
            let qw = new Queue({userID: "5f42d897d939e23060683c02"})
            qw.save().then(()=>{
                resolve()
            })
        })
    }
    setAPIS(userID, apiKey, apiSecret) {
        return new Promise((resolve, reject)=>{
            Apis.findOne({userID}, (err, res)=>{
                if(err || !res){
                    let appo = new Apis({userID, apiKey, apiSecret})
                    appo.save().then(()=>{
                        resolve()
                    })
                } else {
                    Apis.findOneAndUpdate({userID}, {"$set": {apiKey, apiSecret}}, (error, resp)=>{
                        resolve()
                    })
                }

            }) 
            
        })
    }
    getAPIS(userID) {
        return new Promise((resolve, reject)=>{
            Apis.findOne({userID}, (err, res)=>{
                if(err || !res){
                    reject()
                } else {
                    resolve(res)
                }

            }) 
            
        })
    }
    getSignals() {
        return new Promise((resolve, reject)=>{
            Signal.find({}, (err, res)=>{
                if(err){
                    reject()
                } 
                resolve(res)
            }) 
        })
    }
    getSignalTrades(signalID) {
        return new Promise((resolve, reject)=>{
            OrdersQueue.find({signalID}, (err, res)=>{
                if(err){
                    reject()
                } 
                resolve(res)
            }) 
        })
    } 

    getSignal(signalID) {
        return new Promise((resolve, reject)=>{
            Signal.findById(signalID, (err, res)=>{
                if(err){
                    reject()
                } 
                resolve(res)
            }) 
        })
    }

    async activateBot(userID){
        try{
            const user = await Users.getuser(userID);
            if(!user){
                throw {message: "El usuario no existe"}
            }
            if(!user.fullAccount){
                throw {message: "La cuenta del usuario no esta activada"}
            }
            const metric = await Metrics.findOne({userID});
            if(!metric){
                await Metrics.create({userID, 
                    fullAccount: user.fullAccount,
                    isBotActive: true,
                    benefitsPercent: 0,
                    signalsCounter: 0});
            }else{
                await Metrics.findOneAndUpdate({userID}, {isBotActive: true});
            }
            return true;
        } catch (error) {
            console.log("error at activateBot", error.message);
            return false;
        }
    }

    async desactivateBot(userID){
        try{
            let metric = await Metrics.findOne({userID});
            if(!metric){
                throw {message: "El usuario no tiene metricas"};
            }else{
                await Metrics.findOneAndUpdate({userID}, {isBotActive: false});
            }
            return true;
        } catch (error) {
            console.log("error at desactivateBot", error.message);
            return false;
        }
    }

    async rotateQueue(){
        try {
            ////// Test aumentar beneficios

            const usersToChangebenefits = await Queue.find({});
            for (const user of usersToChangebenefits) {
                const metric = await Metrics.findOne({userID: user.userID});
                await Metrics.findOneAndUpdate({userID: metric.userID}, {benefitsPercent: metric.benefitsPercent + parseInt(Math.random() * (6 - 1) + 1)});
            }

            //////

            await Queue.deleteMany({});
            const metrics = await Metrics.find({fullAccount: true, isBotActive: true}).sort({benefitsPercent: 1}).limit(2);
            const metricsToQueue = metrics.map(
                (metric) => {
                    return {userID: metric.userID, benefitsPercent: metric.benefitsPercent};
                }
            );
            await Queue.create(...metricsToQueue);
            return true;
        } catch (error) {
            console.log("error at rotateQueue", error.message);
            return false;
        }
    }
}

module.exports = new Queues()