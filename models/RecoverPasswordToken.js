var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var RecoverPasswordTokenSchema = Schema(
    {
        email: String,
        phone: String,
        token: String,
        active: Boolean,
        expiresIn: Date,
    },
    { timestamps: true }
);
RecoverPasswordTokenSchema;
module.exports = mongoose.model("RecoverPasswordTokens", RecoverPasswordTokenSchema);
