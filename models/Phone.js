var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var DiscountSchema = Schema({
    phone: String,
    used: Boolean,
}, {timestamps: true});

module.exports = mongoose.model('Discount', DiscountSchema);