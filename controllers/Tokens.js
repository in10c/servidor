var Token = require("../models/Token")
var Phone = require("../models/Phone")
const {sendMail} = require("../services/mailer")
const {generateCode} = require("../services/codeGenerator")
const {encrypt, decrypt} = require("../services/encryptor")
const Constants = require("../services/constants")
const Payments = require ("../controllers/Payments")
const Path = require("../models/Path")

const charGroup = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

const accountSid = true ? "AC424ef72df1be3a7f774a85a5a199bb60" : 'AC8379e5d71497f4ea1aa4c30103f50ed0';
const authToken = true ? 'd2d37bb70b7c4b7a48b4a7ccd2f2d793' : "68054670f1804929922cd6753460a7a1";
const clientSMS = require('twilio')(accountSid, authToken);


class Tokens {
    generateToken(){
        return generateCode(charGroup, 5, "", [])
    }
    generateSMSCode(){
        return generateCode(charGroup, 6, "",   [])
    }
    generateEmailCode(){
        return generateCode(charGroup, 6, "",   [])
    }
    saveToken(newToken, insertID, selectedPackage, halfDiscount,  phone, refererID, infoClient){
        let smsCode = this.generateSMSCode()    
        return new Promise((resolve, reject)=>{
            clientSMS.messages.create({
                body: 'El código SMS Bitwabi es el siguiente '+smsCode,
                from: '+19705172699',
                to: "+"+phone
            })
            .then(message => {
                console.log("se envio el mensaje", message)
                
                let TKN = new Token({
                    token: newToken,
                    smsCode,
                    userID: insertID, infoClient,
                    refererID,
                    selectedPackage,
                    mobile: phone,
                    halfDiscount, reservation: true,
                    active: true, stepPhonePassed: false, confirmedPhone: false,
                    //wif : encrypt(wif)
                })
                TKN.save().then(()=>{
                    resolve({token: newToken})
                })
                
            }, err=>{
                console.log("error msj confirm", err)
                reject("Hubo un error al enviar el sms de confirmación: "+err.message)
            });
        })
    }
    findAndUpdate(token, data){
        return new Promise((resolve, reject)=>{
            Token.findOneAndUpdate({token}, data, (err, res)=>{
                if(err){
                    console.log("error al traer categorias: ", err)
                    reject()
                }
                resolve(data)
            })
        })
    }
    findAndUpdatePhone(phone, data){
        return new Promise((resolve, reject)=>{
            var numbers = phone.replace(/\D/g, "");
            Phone.findOneAndUpdate({phone: numbers}, data, (err, res)=>{
                if(err){
                    console.log("error al actualizar phone: ", err)
                    reject()
                }
                resolve(data)
            })
        })
    }
    searchPhone(phone){
        return new Promise((resolve, reject)=>{
            
            console.log("venimos a sbucar telefono", phone)
            Phone.findOne({phone}, (err, res)=>{
                if(err || !res){
                    console.log("error al traer phone: ", err)
                    reject()
                }
                console.log("si se pudo", res)
                resolve(res)
            } )
        })
    }
    getToken(tkn){
        return new Promise((resolve, reject)=>{
            //console.log("entramos")
            Token.findOne({token: tkn}, (err, res)=>{
                if(err){
                    console.log("error al traer categorias: ", err)
                    reject()
                }
                //console.log("si se pudo", res)
                resolve(res)
            })
        })
    }
    getTokenByUser(userID){
        return new Promise((resolve, reject)=>{
            //console.log("entramos")
            Token.findOne({userID}, (err, res)=>{
                if(err){
                    console.log("error al traer usuario por token: ", err)
                    reject()
                }
                //console.log("si se pudo", res)
                resolve(res)
            })
        })
    }
    async fixUserData(token, refererID){
        try {
            const tokenToUpdate = await Token.findOne({token})
            if(!tokenToUpdate){
                return {result: false, message: "El token no existe"}
            }
            if(tokenToUpdate.userID == refererID){
                return {result: false, message: "El rereferID y el userID no pueden ser los mismos"}
            }
            const userPayments = await Payments.getMyPayments(tokenToUpdate.userID);
            if(userPayments.length === 0){
                return {result: false, message: "El usuario no tiene ningun pago registrado"}
            }
            if(tokenToUpdate.payed && !tokenToUpdate.active && tokenToUpdate.txhash){
                return {result: false, message: "Este token ya fue configurado"}
            }
            const {tx} = userPayments.filter((payment) => payment.description == "Reservación de licencia")[0]
            await Token.findOneAndUpdate({token}, {
                refererID,
                payed: true,
                active: false,
                txid : tx,
                txhash: "TRANSACCION DETECTADA Y REGISTRO FALLIDO - REGISTRO MANUAL"
            })
            return {result: true, userID: tokenToUpdate.userID}
        } catch (error) {
            console.log(error);
            return {result: false, message: error.message}
        }
    }
    async verifyHalfDiscount (userID){
        try {
            return !!((await Token.findOne({userID})).halfDiscount)
        } catch (error) {
            console.log("error at verifyHalfDiscount", error)
            return false
        }
    }
    async updateReferer(token, refererID){
        try {
            const tokenToUpdate = await Token.findOne({token})
            if(!tokenToUpdate){
                return {result: false, message: "El token no existe"}
            }
            if(tokenToUpdate.refererID == refererID){
                return {result: false, message: "El rereferID es el mismo que el anterior de este usuario"}
            }
            if(tokenToUpdate.userID == refererID){
                return {result: false, message: "El rereferID y el userID no pueden ser los mismos"}
            }
            const userPath = await Path.findOne({userID:tokenToUpdate.userID})
            if(!userPath){
                return {result: false, message: "El usuario no tiene path"}
            }
            // if(userPath.path.length === 0){
            //     return {result: false, message: "Operación no permitida, el usuario es el nodo principal de un arbol"}
            // }
            await Token.findOneAndUpdate({token}, {
                refererID
            })
            return {result: true, userID: tokenToUpdate.userID}
        } catch (error) {
            console.log(error);
            return {result: false, message: error.message}
        }
    }
}

module.exports = new Tokens()