var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var PaymentSchema = Schema({
    userID: String,
    description: String,
    quantity: Number,
    quantityBTC: Number,
    selectedPackage: Number,
    discount: String,
    priceBTCinEuros: Number,
    tx: String
}, {timestamps: true});

module.exports = mongoose.model('Payment', PaymentSchema);