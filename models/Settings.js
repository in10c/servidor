var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var SettingsSchema = Schema({
    kind: String,
    value: Boolean,
}, {timestamps: true});

module.exports = mongoose.model('Settings', SettingsSchema);