var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var PathSchema = Schema({
    userID: String,
    path: Array,
}, {timestamps: true});

module.exports = mongoose.model('Paths', PathSchema);