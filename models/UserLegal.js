var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var UserLegalSchema = Schema({
    userID: String,
    exonerationResponsabilityAgreement: Boolean,
    exonerationResponsabilityAgreementDate: Date,
    termsAndConditionsAndPrivacyPoliciesAgreement: Boolean,
    termsAndConditionsAndPrivacyPoliciesAgreementDate: Date,
    legalWarningAgreement: Boolean,
    legalWarningAgreementDate: Date
}, {timestamps: true});

module.exports = mongoose.model('UserLegal', UserLegalSchema);