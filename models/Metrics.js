var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var MetricsSchema = Schema({
    userID: String,
    fullAccount: Boolean,
    isBotActive: Boolean,
    benefitsPercent: Number,
    signalsCounter: Number,
}, {timestamps: true});

module.exports = mongoose.model('Metrics', MetricsSchema);