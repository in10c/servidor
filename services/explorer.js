var explorers = require('litecore-explorers');
var insight = new explorers.Insight('https://insight.litecore.io', 'mainnet');

class ExplorerClass {
    getData(address){
        return new Promise((resolve, reject)=>{
            insight.address(address, (err, utxos) => {
                if (err) {
                    console.log("teme,ps errpr", err)
                    reject(err)
                } else {
                    console.log("todo funciono ", utxos)
                    resolve(utxos.balance/100000000)
                }
            });
        })
    }
    getUtxos(address){
        return new Promise((resolve, reject)=>{
            insight.getUtxos(address, (err, utxos) => {
                if (err) {
                    console.log("teme,ps errpr", err)
                    reject(err)
                } else {
                    console.log("todo funciono ", utxos)
                    resolve(utxos)
                }
            });
        })
    }
    broadcastTx(transaction){
        return new Promise((resolve, reject)=>{
            insight.broadcast(transaction, (err, returnedTxId) => {
                if (err) {
                    console.log("NOOO se broadcasteo ", err)
                    reject(err)
                } else {
                    console.log("se broadcasteo ", returnedTxId)
                    resolve(returnedTxId)
                }
            });
        })
    }   
}
module.exports = new ExplorerClass();