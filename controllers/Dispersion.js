const bitcoin = require('bitcoinjs-lib')
var axios = require("axios").default
var Path = require("../models/Path")
const Upgrade = require("../models/Upgrade")
const Activation = require("../models/Activation")
const Constants = require("../services/constants")
const Payments = require("../controllers/Payments")
const Users = require("../controllers/Users")
const {fixedToMultiplier} = require("../services/numbers")
const {getAddressInputs, getBTCPrice, generatePrivate} = require("./Bitcoin")
const Family = require('./Family')
const accountSid = true ? "AC424ef72df1be3a7f774a85a5a199bb60" : 'AC8379e5d71497f4ea1aa4c30103f50ed0';
const authToken = true ? 'd2d37bb70b7c4b7a48b4a7ccd2f2d793' : "68054670f1804929922cd6753460a7a1";
const clientSMS = require('twilio')(accountSid, authToken);

class Dispersion {
    disperse(amount, userID, address, privateKey, topayEuro,priceBTCinEuros, newPackage){
        return new Promise(async (resolve, reject)=>{
            try {
                // let {company, vault, pendingPayments} = await this.getDividedAmount(amount, userID)   
                // console.log("en disperse,", company, vault)
                // let {txHex, txid} = await this.disperseCompany(company, address, privateKey, vault)
                let {txHex, txid} = await this.saveAllAmount(amount, address, privateKey)
                let pendingPayments = []
                let vault = 0, company = 0
                console.log("terminamos de dispersar", txid)
                await this.activateAccount(txHex, txid, topayEuro, priceBTCinEuros, newPackage, company, vault, amount, userID, pendingPayments)
                resolve()             
            } catch (error) {
                console.log("el eror ", error)                
                reject()
            }
        })
    }
    async disperseCompany(amount, address, privateKey, vault){
        //get keypairs by private
        const keyPair = bitcoin.ECPair.fromWIF(privateKey, Constants.globalNetwork())
        //amounts calculation
        let todisperse = amount - Constants.getBlockchainFee()
        let pctgBig = (todisperse * .24).toFixed(0)
        let pctgMin = (todisperse * .04).toFixed(0)
        //instatiate psbt
        const psbt = new bitcoin.Psbt({network: Constants.globalNetwork()})
        //get all inputs, add and sign
        let txInputs = await getAddressInputs(address)
        let counterInput = 0
        for (let index = 0; index < txInputs.length; index++) {
            psbt.addInput(txInputs[index])
            counterInput += 1
        }
        //add outputs
        psbt.addOutput({ address: Constants.walletCristian(),  value: parseInt(pctgBig) }) 
        .addOutput({ address: Constants.walletYhon(), value: parseInt(pctgBig) })
        .addOutput({ address: Constants.walletOscar(), value: parseInt(pctgBig) })
        .addOutput({ address: Constants.walletAlex(), value: parseInt(pctgBig) })
        .addOutput({  address: Constants.walletDiego(), value: parseInt(pctgMin) });
        //finally we return the commissions money
        if(vault){
            psbt.addOutput({address, value: vault})
        }
        //sign all inputs
        for (let indxInpt = 0; indxInpt < counterInput; indxInpt++) {
            //tx sign
            psbt.signInput(indxInpt, keyPair)
            psbt.validateSignaturesOfInput(indxInpt, keyPair.publicKey)
        }
        //finalize and broadcast
        psbt.finalizeAllInputs()
        let hex = psbt.extractTransaction().toHex()
        let resp = await axios.post(Constants.blockstream()+'/api/tx', hex)
        console.log("error de broadcats", resp)
        return {txHex: hex, txid: resp.data}
    }
    async saveAllAmount(amount, address, privateKey){
        //get keypairs by private
        const keyPair = bitcoin.ECPair.fromWIF(privateKey, Constants.globalNetwork())
        //amounts calculation
        let todisperse = amount - Constants.getBlockchainFee()
        //instatiate psbt
        const psbt = new bitcoin.Psbt({network: Constants.globalNetwork()})
        //get all inputs, add and sign
        let txInputs = await getAddressInputs(address)
        let counterInput = 0
        for (let index = 0; index < txInputs.length; index++) {
            psbt.addInput(txInputs[index])
            counterInput += 1
        }
        //add outputs
        psbt.addOutput({ address: Constants.walletCommunitaria(),  value: todisperse }) 
        //sign all inputs
        for (let indxInpt = 0; indxInpt < counterInput; indxInpt++) {
            //tx sign
            psbt.signInput(indxInpt, keyPair)
            psbt.validateSignaturesOfInput(indxInpt, keyPair.publicKey)
        }
        //finalize and broadcast
        psbt.finalizeAllInputs()
        let hex = psbt.extractTransaction().toHex()
        let resp = await axios.post(Constants.blockstream()+'/api/tx', hex)
        console.log("error de broadcats", resp)
        return {txHex: hex, txid: resp.data}
    }
    getInputs(hash){
        return new Promise((resolve, reject)=>{
            axios.get(Constants.blockstream()+"/api/tx/"+hash+"/hex").then(urlTxes=>{
                urlTxes = urlTxes.data
                console.log("se pondra hex", urlTxes)
                resolve(Buffer.from(urlTxes, 'hex'))
            }) 
        })
    }
    broadcast(txHex) {
        return new Promise((resolve, reject)=>{
            axios.post(Constants.blockstream()+'/api/tx', txHex).then( resp => {
                console.log("transacction submited", resp, resp.data)
                resolve({txHex, txid: resp.data})
            }).catch( err => {
                console.log("hubo un error, se repetira en 30 segu", err)
                reject()
            })
        })
    }
    async activateAccount(txHex, txid, topayEuro,priceBTCinEuros, selectedPackage, company, vault, amount, userID, pendingPayments){
        //change from satoshis to btc
        let amountInBTC = fixedToMultiplier((amount / 100000000), 100000000)
        await Payments.activationPayment(userID, "Activación de cuenta", topayEuro, amountInBTC, selectedPackage, 0, txid, priceBTCinEuros)
        console.log("se activo la cuenta")
        await Users.updateActivation(userID, {
            dispersedToCompany: company,
            remainInWallet: vault,
            payed: true,
            active: false,
            txhash: txid,
            pendingDispersion: pendingPayments
        })
        await Users.findAndUpdate(userID, {
            fullAccount:true, reservation: false, selectedPackage, downgrade: true
        })
        const user = await Users.getuser(userID)
        await Family.updateMyFamily(user._id.toString(), user.name, user. lastName, user.nickName, user.email, selectedPackage, user.fullAccount)
        await clientSMS.messages.create({
            body: 'Se ha confirmado el pago completo de su cuenta Bitwabi, ahora su cuenta está activa. Muchas gracias.',
            from: '+19705172699',
            to: Constants.production ? "+"+user.phone : "+525543671245"
        })
    }
    getDividedAmount(amount, userID){
        return new Promise(async (resolve, reject)=>{
            try {
                let auxPath = await Path.findOne({userID})
                let userPath = auxPath.path
                let firstBonus = parseFloat((30 * amount / 75).toFixed(0))
                let miniBonus = parseFloat((2 * amount / 75).toFixed(0))
                let pendingPayments = []
                console.log("eel path aux es ", userPath, amount, firstBonus, miniBonus)
                let vault = 0
                // si el usuario no es raiz 
                if(userPath.length > 0){
                    let lastten = userPath.slice(-5)
                    let first = true
                    do {
                        let item = lastten.pop()
                        if(first){
                            vault += firstBonus
                            pendingPayments.push({address: item, quantity: firstBonus}) 
                        } else {
                            vault += miniBonus
                            pendingPayments.push({address: item, quantity: miniBonus}) 
                        }
                        first = false
                    } while (lastten.length > 0);
                }
                let company = amount - vault
                console.log("a la compania se le mete", company, vault) 
                resolve({company, vault, pendingPayments})
            } catch (error) {
                console.log("el eror ", error)                
                reject()
            }
            
        })
    }
    async createUpgradeTkn(userID, infoClient, newPackage){
        console.log("isers es ", Users)
        let profile = await Users.getProfile(userID)
        let today = new Date()
        console.log("la profile me da ", profile)
        let {diffPayment, diffPaymentBTC, priceBTCinEuros, payedOld, toPayForNewOne} = await this.getUpgradePrice(profile.selectedPackage, profile.halfDiscount, newPackage)
        console.log("trago de get upgrade price",  diffPayment, diffPaymentBTC, priceBTCinEuros, payedOld, toPayForNewOne)
        let {address, wif} = generatePrivate()
        console.log("desps de private  ", address, wif)
        
        let response = await new Upgrade({
            userID, address,
            quantity: diffPaymentBTC,
            oldPackage: profile.selectedPackage,
            newPackage: newPackage, payedOld, toPayForNewOne,
            generatedTime: today, diffPayment,
            halfDiscount: profile.halfDiscount,
            priceBTCinEuros,
            payed: false,
            active: true,
            wif,
            infoClient,
            status: "No dispersada" 
        }).save()
        return { address, quantity: diffPaymentBTC, priceBTCinEuros, diffPayment, 
            halfDiscount: profile.halfDiscount, payedOld, toPayForNewOne, generatedTime: today.getTime(), _id: response._id}
    }
    async getUpgradePrice(oldPackage, halfDiscount, newPackage){
        let priceBTCinEuros = await getBTCPrice()
        let _oldPackage = Constants.pckgInfo(oldPackage)
        let _newPackage = Constants.pckgInfo(newPackage)
        //obtenemos precio pagado por el paquete actual
        let payedOld = halfDiscount ? _oldPackage.half : _oldPackage.price
        //el nuevo paquete tiene descuento ?
        let toPayForNewOne = halfDiscount ? (_newPackage.price * .6) : _newPackage.price
        //buscamos la diferencia 
        let diffPayment = toPayForNewOne - payedOld
        let diffPaymentBTC = (diffPayment / priceBTCinEuros).toFixed(5)
        return {diffPaymentBTC, diffPayment, priceBTCinEuros, payedOld, toPayForNewOne}
    }
    async getUpgradeData(userID){
        return await Upgrade.findOne({userID, active: true})
    }
    async deleteUpgrade(upgradeID){
        await Upgrade.findByIdAndUpdate(upgradeID, {
            active: false
        })
    }
    async activateAccountWOPay( userID, selectedPackage, infoClient ){
        let today = new Date()
        //change from satoshis to btc
        const user = await Users.getuser(userID)
        await new Activation({
            userID,
            quantity: 0, selectedPackage,
            generatedTime: today, remainPayment : 0,
            priceBTCinEuros: 0,
            payed: false,
            active: true,
            infoClient,
            status: "Nada para dispersar, activacion por downgrade" 
        }).save()
        await Payments.activationPayment(userID, "Activación de cuenta con downgrade", 0, 0, selectedPackage, 0, null, 0)
        console.log("se activo la cuenta")
        await Users.updateActivation(userID, {
            payed: true,
            active: false,
        })
        await Users.findAndUpdate(userID, {
            fullAccount:true, reservation: false, selectedPackage, downgrade: true
        })
        
        await Family.updateMyFamily(userID, user.name, user. lastName, user.nickName, user.email, selectedPackage, true)
        await clientSMS.messages.create({
            body: 'Se ha confirmado el pago completo de su cuenta Bitwabi, ahora su cuenta está activa. Muchas gracias.',
            from: '+19705172699',
            to: Constants.production ? "+"+user.phone : "+525543671245"
        })
    }
}

module.exports = new Dispersion()