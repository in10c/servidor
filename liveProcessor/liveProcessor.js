var mongoose = require('mongoose');
var axios = require("axios").default
const bitcoin = require('bitcoinjs-lib')
const Tokens = require("../controllers/Tokens")
const Users = require("../controllers/Users");
const Payments = require('../controllers/Payments');
const {address, privateKey, token, topay, userID, topayEuro,priceBTCinEuros, selectedPackage, refererID} = JSON.parse(process.argv[2])
const Constants = require("../services/constants")

var today = new Date()
var every = 120000
const constructionTime = today.getTime()
const timeout = 300

const BITCOIN_FEE = 10000;

const Yhon = Constants.network === "mainnet" ? "17QUgAomaF6mttzwowqLi786iZTdwsS9un" : "mnfQSfPHef9E48QtMWRtRhgMQ3KuXs4osg"
const Alex = Constants.network === "mainnet" ? "bc1qzqwljxh680rf603fdfgkrjwkltyzexe53w07ld" : "tb1qvjw0tyr6rc7hz400nkx4tl2es3qrqn3v8mfywd"
const Oscar = Constants.network === "mainnet" ? "34G5hsmbzWPUavYtjSinDhkMRCs5AmBiPz" : "tb1qthkufzaxdfvymw6hjpd5w5eva5jlqx6ygjma7g"
const Cristian = Constants.network === "mainnet" ? "3KupJ9c13xngBxmjNLgECNCY7sYt7yuTbj" : "mznGJRvZaDZ3HvDZe1RyExQiNLwUaj6KFT"
const Diego = Constants.network === "mainnet" ? "3DS3gaBzSqVRKTUXQHHMfTaKAoTsQMUyUe" : "tb1qnq3z8e5zg9hptq9z8ngdh0eew84jlms5preenm"

const network = Constants.network === "mainnet" ? bitcoin.networks.bitcoin : bitcoin.networks.testnet

const blockchainInfoURL = Constants.network === "mainnet" ? "https://blockchain.info" : "https://testnet.blockchain.info"
const blockstream = Constants.network === "mainnet" ? "https://blockstream.info" : "https://blockstream.info/testnet"

function getBalance(address) {
    console.log("obtendre bvalance de ", address)
    axios.get(blockstream+'/api/address/' + address).then((resp)=>{
        console.log("la resp es ", resp.data, resp.data.chain_stats.funded_txo_sum)
        let satoshis = topay * 100000000
        if( resp.data.chain_stats.funded_txo_sum > 0 && resp.data.chain_stats.funded_txo_sum >= satoshis){
            //console.log("si paso a dispersas--->>>", resp.data.final_balance)
            disperse(resp.data.chain_stats.funded_txo_sum)
        } else {
            repeat(address)
        }
    }, err=>{
        console.log("error", err)
        repeat(address)
    })
}

const getElapsedTime = (time) =>{
    return Math.floor(time / 60000);
}

function repeat(address) {
    let now = new Date().getTime()
    let elapsedTime = now - constructionTime
    let minutes = getElapsedTime(elapsedTime)
    console.log("vamos a repetir", minutes, timeout)
    if(minutes > timeout){
        connectDatabase(()=> {
            Tokens.findAndUpdate(token, {
                payed: false,
                active: false
            })
        })
    } else {
        setTimeout(()=>{
            getBalance(address)
        }, every)
    }
}

function connectDatabase(cb) {
    mongoose.Promise = global.Promise;
    // Usamos el método connect para conectarnos a nuestra base de datos
    mongoose.connect(Constants.databaseStringConn(), { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
    .then(() => {
        cb()
    })
    .catch(err => console.log(err));
}

function getInputs(hash){
    return new Promise((resolve, reject)=>{
        axios.get(blockstream+"/api/tx/"+hash+"/hex").then(urlTxes=>{
            urlTxes = urlTxes.data
            console.log("se pondra hex", urlTxes)
            resolve(Buffer.from(urlTxes, 'hex'))
        }) 
    })
}

function disperse(amount){
    axios.get(blockstream+'/api/address/' + address+"/txs").then((respTXS)=>{
        console.log("la resp es ", respTXS.data)
        //genero key mediante wif
        const keyPair = bitcoin.ECPair.fromWIF(privateKey, network)
        let inputTX = respTXS.data[0]
        console.log("el poutput es ", inputTX.vout)
        let voutN;
        inputTX.vout.map((val, indx)=>{
            if(val.scriptpubkey_address === address){
                voutN = indx
            }
        })
        console.log("el val elegido es  ", voutN)
       
        let blockchainFee = 10000
        let todisperse = amount - blockchainFee
        let pctgBig = (todisperse * .24).toFixed(0)
        let pctgMin = (todisperse * .04).toFixed(0)
        console.log("revision", blockchainFee, todisperse, pctgBig, pctgMin)
        getInputs(inputTX.txid).then((nonWitnessUtxo)=>{
            //creo transaccion
            console.log("entro, ", {
                address: Cristian,
                value: pctgBig,
            }, Cristian, pctgMin, pctgBig, Yhon, Oscar, Alex, Diego)
            const psbt = new bitcoin.Psbt({network})
            .addInput({
                hash: inputTX.txid,
                index: voutN,
                nonWitnessUtxo,
            }) 
            .addOutput({
                address: Cristian,
                value: parseInt(pctgBig),
            }) 
            .addOutput({
                address: Yhon,
                value: parseInt(pctgBig),
            })
            .addOutput({
                address: Oscar,
                value: parseInt(pctgBig),
            })
            .addOutput({
                address: Alex,
                value: parseInt(pctgBig),
            })
            .addOutput({
                address: Diego,
                value: parseInt(pctgMin),
            })
            //firmo transaccion
            psbt.signInput(0, keyPair)
            psbt.validateSignaturesOfInput(0, keyPair.publicKey)
            psbt.finalizeAllInputs()
            console.log('Transaction hexadecimal:')
            console.log(psbt.extractTransaction().toHex())
            broadcast(psbt.extractTransaction().toHex())
        })
    }, err=>{
        console.log("error", err)
        repeat(address)
    })
    
}
const activateAccount = async (txHex, txid) =>{
    await Payments.registerNewPayment(userID, "Reservación de licencia", topayEuro, topay, selectedPackage, 0, txid, priceBTCinEuros)
    await Users.findAndUpdate(userID, {
        active:true,
        reservation: true
    })
    await Users.createPath(userID, refererID)
    await Tokens.findAndUpdate(token, {
        payed: true,
        active: false,
        txhash: txHex
    })
}

const errorFunction = (txHex) => {
    Tokens.findAndUpdate(token, {
        payed: true,
        active: false,
        txhash: txHex,
        retrying: true
    }).then(()=>{
        console.log("registraremos el reintento dentro de 30 segundos")
        setTimeout(()=>{
            broadcast(txHex)
        }, 30000)
    })
}

function broadcast(txHex) {
    axios.post(blockstream+'/api/tx', txHex).then( resp => {
        console.log("transacction submited", resp, resp.data)
        activateAccount(txHex, resp.data)
    }).catch( err => {
        console.log("hubo un error", err)
        errorFunction(txHex)
    })
}

setTimeout(()=>{
    connectDatabase(()=>getBalance(address))
}, every)