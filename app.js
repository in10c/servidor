var express = require('express');
var bodyParser = require('body-parser')
var cors = require('cors')
const Binance = require('node-binance-api');

//import controllers
const Tokens = require("./controllers/Tokens")
const Users = require("./controllers/Users")
const Family = require("./controllers/Family")
const Queues = require("./controllers/Queues")
const OrderKeepers = require("./controllers/OrderKeepers")
const ProtectedBalances = require("./controllers/ProtectedBalances")
const Dispersion = require("./controllers/Dispersion")
const Payments = require('./controllers/Payments');
const UserSettings = require("./controllers/UserSettings")
const Activations = require("./controllers/Activations")
const RecoverPasswordTokens = require("./controllers/RecoverPasswordTokens")
const Setting = require("./models/Settings")
const { generateAddress, manualPaymentProcess, getBalance, broadcast, generateUser, generateActivation, getBalancePro, sendCommissions } = require("./controllers/Bitcoin");
const Token = require('./models/Token');
const Path = require('./models/Path');

const { verifyAuthToken, generateAuthToken } = require('./services/jsonWebToken')
const verifyDeveloperKey = require('./services/verifyDeveloperKey');
const Constants = require('./services/constants');
const Tree = require('./models/Tree');


var app = express();
var jsonParser = bodyParser.json()

app.use(cors())

app.get('/testAuth', verifyAuthToken, function (req, res) {
    res.json({message: `hello ${req.body.userID}`});
});

app.get('/testDevKey', verifyDeveloperKey, function (req, res) {
    res.json({message: `Happy hacking :)`});
});

app.post('/testExistUser', verifyDeveloperKey, jsonParser, async function (req, res) {
    const { userID } = req.body
    res.send({result: await Users.existsUser(userID)})
})

app.post("/testQueueRotation", verifyDeveloperKey, async (req, res) => {
    res.send({result:await Queues.rotateQueue()})
})

app.post('/testGetChilds', verifyDeveloperKey, jsonParser, async function (req, res) {
    const { userID } = req.body
    res.send({result: await Family.getTree(userID)})
})

app.get('/testGetChilds', jsonParser, verifyAuthToken, async function (req, res) {
    const { userID } = req.body
    const family = await Family.getTree(userID)
    if(family){
        res.send({success: 1, family})
    }else{
        res.send({success: 0, message: "Lo sentimos, ha ocurrodi un error"})
    }
    
})

app.post('/addHalfDiscountToTree', verifyDeveloperKey, jsonParser, async function (req, res) {
    const paths = await Path.find()
    for(path of paths){
        const user = await Users.getByID(path.userID)
        await Family.updateMyFamily(user._id.toString(), user.name, user.lastName, user.nickName, user.email, user.selectedPackage, user.fullAccount)
    }
    res.send("done")
})

app.get('/', function (req, res) {
    res.send("Nothing to see.<br/><br/>Go away from here...");
});

app.post("/procesaPagoManual", verifyDeveloperKey, jsonParser, function (req, res) {
    let { address } = req.body
    Token.findOne({address}, (err, tkn)=>{
        if(err){
            res.send({success: 0, message: "No se encontro token con esa direccion"})
        }
        console.log("trajo el primero ", tkn)
        manualPaymentProcess(tkn).then((hex)=>{
            broadcast(hex, tkn).then(()=>{
                res.send({success: 1})
            })
        })
    }) 
})

app.post("/generateUser", jsonParser, verifyDeveloperKey, async function (req, res) {
    let { tkn } = req.body
    let tokn = await Tokens.getToken(tkn)
    console.log("el token ", tokn)
    await Payments.registerNewPayment(tokn.userID, "Reservación de licencia", tokn.reservationPayment, tokn.quantity, tokn.selectedPackage, 0, "TRANSACCION NO DETECTADA - REGISTRO MANUAL", tokn.priceBTCinEuros)
    await Tokens.findAndUpdate(tokn.token, {
        payed: true,
        active: false,
        txhash: "TRANSACCION NO DETECTADA - REGISTRO MANUAL"
    })
    await Users.findAndUpdate(tokn.userID, {
        active:true,
        reservation: true
    })
    await Users.createPath(tokn.userID, tokn.refererID)
    res.send({success: 1})
})

app.post("/fixUserData", verifyDeveloperKey, jsonParser, async function (req, res) {
    const { token, userID, refererID } = req.body
    if(!refererID){
        res.send({success: 0, message: "No has proporcionado el refererID"})
    }
    else {
        if(await Users.existsUser(refererID)){
            if (token) {
                const resultFixToken = await Tokens.fixUserData(token, refererID)
                if(resultFixToken.result){
                    await Users.createPath(resultFixToken.userID, refererID)
                    res.send({success: 1, message: "Actualizado con exito"})
                }else{
                    res.send({success: 0, message: resultFixToken.message})
                }
            }else if(userID){
                if(await Users.existsUser(userID)){
                    const userToken = await Tokens.getTokenByUser(userID)
                    if(userToken){
                        const resultFixToken = await Tokens.fixUserData(userToken.token, refererID)
                        if(resultFixToken.result){
                            await Users.createPath(resultFixToken.userID, refererID)
                            res.send({success: 1, message: "Actualizado con exito"})
                        }else{
                            res.send({success: 0, message: resultFixToken.message})
                        }
                    }else{
                        res.send({success: 0, message: "El usuario no tiene token"})
                    }
                }
                else{
                    res.send({success: 0, message: "No existe el usuario con el userID " + refererID})
                }
            }else{
                res.send({success: 0, message: "No has proporcionado un token o el userID"})
            }
        }
        else{
            res.send({success: 0, message: "No existe el usuario con el refererID " + refererID})
        }
    }
})

app.post("/changeRefererUser", verifyDeveloperKey, jsonParser, async function (req, res) {
    const { token, userID, refererID } = req.body
    if(!refererID){
        res.send({success: 0, message: "No has proporcionado el refererID"})
    }
    else {
        if(await Users.existsUser(refererID)){
            if (token) {
                const resultUpdateReferer = await Tokens.updateReferer(token, refererID)
                if(resultUpdateReferer.result){
                    await Users.changePath(resultUpdateReferer.userID, refererID)
                    res.send({success: 1, message: "Actualizado con exito"})
                }else{
                    res.send({success: 0, message: resultUpdateReferer.message})
                }
            }else if(userID){
                if(await Users.existsUser(userID)){
                    const userToken = await Tokens.getTokenByUser(userID)
                    if(userToken){
                        const resultUpdateReferer = await Tokens.updateReferer(userToken.token, refererID)
                        if(resultUpdateReferer.result){
                            await Users.changePath(resultUpdateReferer.userID, refererID)
                            res.send({success: 1, message: "Actualizado con exito"})
                        }else{
                            res.send({success: 0, message: resultUpdateReferer.message})
                        }
                    }else{
                        res.send({success: 0, message: "El usuario no tiene token"})
                    }
                }
                else{
                    res.send({success: 0, message: "No existe el usuario con el userID " + refererID})
                }
            }else{
                res.send({success: 0, message: "No has proporcionado un token o el userID"})
            }
        }
        else{
            res.send({success: 0, message: "No existe el usuario con el refererID " + refererID})
        }
    }
})

//Deprecated
app.post("/disperseOrder", jsonParser, function (req, res) {
    let { inPair, outPair, sl, enterprice, gain, type } = req.body
    OrderKeepers.disperseOrder(inPair, outPair, sl, enterprice, gain, type ).then(()=>{
        res.send({success: 1})
    }, message=>{
        res.send({success: 0, message})
    })
})

app.post("/getAPIS", jsonParser, verifyAuthToken, function (req, res) {
    let { userID } = req.body
    
    Queues.getAPIS(userID).then((apis=>{
        res.send({success: 1, apis})
    }))
})

app.post("/prueba", jsonParser, function (req, res) {
    let { userID } = req.body
    
    Dispersion.disperse(22000, userID).then(()=>{
        res.send({success: 1})
    })
})

app.post("/getProfile", jsonParser, verifyAuthToken, function (req, res) {
    let { userID } = req.body
    
    Users.getProfile(userID).then((user=>{
        res.send({success: 1, user})
    }))
})

//seccion de activacion

app.post('/generateRemainAddress', jsonParser, verifyAuthToken, function (req, res) {
    let { userID, infoClient, newSelectedPackage } = req.body
    console.log("isers es ", Users, userID, infoClient, newSelectedPackage)
    Users.getProfile(userID).then(profile=>{
        generateActivation(userID, infoClient, profile, newSelectedPackage).then((resp)=>{
            console.log("llego al final")
            res.send({success: 1})
        }, err=>{
            console.log("ocurrio error", err)
            res.send({success: 0})
        })
    })
    
}) 

app.post('/confirmPaymentAndActivate', jsonParser, verifyAuthToken, async function (req, res) {
    let { userID } = req.body
    let activation = await Users.getActivationData(userID)
    console.log("de activacion", activation)
    let {balance, mempool} = await getBalancePro(activation.address)
    console.log("de balance", balance)
    if(balance === 0 && mempool === 0){
        res.send({success: 0, message: "Aún no se ha confirmado su transacción, por favor intente más tarde."})
    } else if(balance === 0 && mempool > 0){
        res.send({success: 0, message: "Su transacción ha sido detectada pero aún no se encuentra confirmada, por favor intente más tarde."})
    } else {
        //si ya hay una cantidad en la billetera
        let quantityToFind = Constants.network === "mainnet" ? activation.quantity : 0.00022
        if(balance >= quantityToFind){
            console.log("si coincidio la cantidad, vamos a dispersar", balance, quantityToFind)
            await Dispersion.disperse(balance, userID, activation.address, activation.wif, activation.remainPayment, activation.priceBTCinEuros, activation.selectedPackage)
            res.send({success: 1})
        } else {
            console.log("no coincidio la cantidad, vamos a dispersar", balance, quantityToFind)
            res.send({success: 0, message: "La cantidad enviada ha sido menor a la cantidad requerida para la activación."})
        }
    }
})
app.post('/downAndActivate', jsonParser, verifyAuthToken, function (req, res) {
    let { userID, newPackage, infoClient } = req.body
    Dispersion.activateAccountWOPay(userID, newPackage, infoClient).then(()=>{
        res.send({success: 1})
    })
})
app.post('/getActivationData', jsonParser, verifyAuthToken, function (req, res) {
    let { userID } = req.body
    Users.getActivationData(userID).then(activation=>{
        res.send({success: 1, activation})
    })
})

//finaliza seccion de activacion

app.get("/tradeMonit", function (req, res) {    
    Queues.getSignals().then((signals=>{
        res.send({success: 1, signals})
    }))
})

app.post("/getSignalTrades", jsonParser, function (req, res) {
    let { signalID } = req.body
    Queues.getSignalTrades(signalID).then((trades=>{
        res.send({success: 1, trades})
    }))
})

app.post("/getSignal", jsonParser, function (req, res) {
    let { signalID } = req.body
    Queues.getSignal(signalID).then((signal=>{
        res.send({success: 1, signal})
    }))
})

app.post("/deleteOrder", jsonParser, function (req, res) {
    let { processID } = req.body
    OrderKeepers.deleteOrder(processID).then((()=>{
        res.send({success: 1})
    }))
})
app.post("/deleteOrders", jsonParser, function (req, res) {
    OrderKeepers.deleteOrders().then((()=>{
        res.send({success: 1})
    }))
})
//deprecated
app.post("/editOCO", jsonParser, function (req, res) {
    let { sl, gain, processList } = req.body
    OrderKeepers.editOCO(sl, gain, processList).then((()=>{
        res.send({success: 1})
    }))
})

app.post("/setAPIS", jsonParser, verifyAuthToken, async function (req, res) {
    try {
        const { userID, apiKey, apiSecret } = req.body
        const client = new Binance().options({
            APIKEY: apiKey,
            APISECRET: apiSecret
        })
        await client.account()
        Queues.setAPIS(userID, apiKey, apiSecret).then((apis=>{
            res.send({success: 1})
        }))
    } catch (error) {
        console.log("error at setAPIS", error.body)
        res.send({success: 0})
    }
})

app.get("/anadeCola", function (req, res) {
    Queues.putInQueue().then(()=>{
        res.send({success: 1})
    })
})

app.post("/obtenBalanceInterno", verifyDeveloperKey, jsonParser, function (req, res) {
    let { address } = req.body
    getBalance(address).then((balance)=>{
        res.send({success: 1, balance})
    })
})

app.post('/generatePaymentGate', jsonParser, function (req, res) {
    let { tkn , pckgNumber} = req.body
    generateAddress().then((resp)=>{
        Tokens.findAndUpdate(tkn, {...resp, selectedPackage : pckgNumber, generatedTime: new Date()}).then((data)=>{
            res.send({success: 1, ...data})
        })
    })
})

app.post('/loginUser', jsonParser, function (req, res) {
    let {email, password} = req.body
    console.log(email, password);
    Users.login({email, password, active: true}).then(async (user)=>{
        if(!user.active){
            res.send({success: 0, message: "La cuenta ha sido registrada pero no está activa, por favor realice el pago de su licencia para activar la cuenta."})
        } else {
            if(await UserSettings.verifyTwoFactorActive(user._id.toString())){
                res.send({success: 1, twoFactorAuthentication: true})
            }else{
                const userLegals = await Users.getTermsAndConditionsAgreement(user._id.toString())
                generateAuthToken(user._id.toString(), res)
                res.send({success: 1, user : {
                    id: user._id,
                    email: user.email,
                    selectedPackage: user.selectedPackage,
                    name: user.name,
                    trader: user.trader,
                    developer: user.developer,
                    fullAccount: user.fullAccount,
                    reservation: user.reservation,
                    admin: user.admin,
                    termsAndConditionsAgreement: userLegals
                }})
            }
        }
    }, ()=>{
        res.send({success: 0, message: "El usuario y/o contraseña son incorrectos"})
    })
})

app.post('/getToken', jsonParser, function (req, res) {
    let {token} = req.body
    Tokens.getToken(token).then((tkn)=>{
        res.send({success: 1, token: tkn})
    }, ()=>{
        res.send({success: 0, message: "El Token no existe"})
    })
})

app.get('/statusInscriptions', function (req, res) {
    Setting.findOne({kind: "statusInscriptions"}, (err, resp)=>{
        console.log("res status", resp)
        res.send({success: 1, value: resp.value})
    })
})

app.post('/getFamily', jsonParser, verifyAuthToken, async function (req, res) {
    const { userID } = req.body
    const family = await Family.getTree(userID)
    if(family){
        res.send({success: 1, family})
    }else{
        res.send({success: 0, message: "Lo sentimos, ha ocurrodi un error"})
    }
})

app.post('/confirmSMSCode', jsonParser, function (req, res) {
    let {token, smsCode} = req.body
    Tokens.getToken(token).then((tkn)=>{
        if(tkn.smsCode === smsCode){
            generateAddress(tkn.token, tkn.selectedPackage, tkn.userID, tkn.refererID, tkn.halfDiscount).then((resp)=>{
                let {address, quantity, wif, priceBTCinEuros, reservationPayment} = resp
                console.log("traigo de generate", resp)
                let today = new Date()
                Tokens.findAndUpdate(tkn.token, {
                    address, quantity, wif, stepPhonePassed: true, confirmedPhone: false, generatedTime: today, priceBTCinEuros, reservationPayment
                }).then((respTkn)=>{
                    console.log("traigo de token", respTkn)
                    if(tkn.halfDiscount){
                        Tokens.findAndUpdatePhone(tkn.mobile, {used: true}).then(()=>{
                            res.send({success: 1})    
                        })
                    } else {
                        res.send({success: 1})
                    }
                    
                })
            }) 
        } else {
            res.send({success: 0, message: "El código SMS proporcionado no es el correcto. Verifique el código e inténtelo de nuevo."})
        }
        
    }, ()=>{
        res.send({success: 0, message: "El Token no existe"})
    })
})


app.post('/doSignUp', jsonParser, function (req, res) {
    let {name, email, password, phone, selectedPackage, refererID, infoClient} = req.body
    Users.isNotRegistered(email).then(()=>{
        
        var numbers = phone.replace(/\D/g, "");
        Users.doSignUp(email, name, password, numbers, selectedPackage).then((insertID)=>{
            let newToken = Tokens.generateToken()
            Users.notExistPhone(numbers).then((succ)=>{
                Tokens.searchPhone(numbers).then(resp=>{
                    console.log("si tiene descuento", resp)
                    if(!resp.used){
                        Tokens.saveToken(newToken, insertID, selectedPackage, true, numbers, refererID, infoClient).then((respToken)=>{
                            let {token} = respToken
                            res.send({success: 1, token})
                        }, err=>{
                            res.send({success: 0, message: err})
                        })
                    } else {
                        res.send({success: 0, message: "El descuento con el número telefónico ingresado ya ha sido usado."})
                    }
                //no tiene descuento
                }, err=>{
                    console.log("no tiene descuento", err)
                    Tokens.saveToken(newToken, insertID, selectedPackage, false, numbers, refererID, infoClient).then((respToken)=>{
                        let {token} = respToken
                        res.send({success: 1, token})
                    }, err=>{
                        res.send({success: 0, message: err})
                    })
                })
            }, errNum =>{
                res.send({success: 0, message: "Otra cuenta de Bitwabi ha sido registrada con el número ingresado."})
            })
            
        })
    }, ()=>{
        res.send({success: 0, message: "El correo ingresado ya ha sido registrado previamente."})
    })
    
})

app.get("/getMyChildren", jsonParser, verifyAuthToken, async (req, res) => {
    const { userID } = req.body
    const children = await Family.getTree(userID)
    if(children){
        res.send({success: 1, children: children.childs.length})
    }else{
        res.send({success: 1, children: 0})
    }
})

app.put("/changePassword", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID, oldPassword, newPassword} = req.body
    if(await Users.changePassword(userID, oldPassword, newPassword)){
        res.send({success: 1, message: "Contraseña cambiada correctamente"})
    }else{
        res.send({success: 0, message: "Fallo al cambiar contraseña, compruebe su contraseña anterior"})
    }
})

app.post("/sendEmailVerificationCode", jsonParser, verifyAuthToken, (req, res) => {
    const {userID, email, newEmail} = req.body
    Users.isNotRegistered(newEmail).then(async ()=>{
        const emailCode = Tokens.generateEmailCode()
        if(await UserSettings.saveEmailCode(userID, email, newEmail, emailCode)){
            res.send({success: 1,  message: "Se ha enviado el código de verificación a " + newEmail})
        } else {
            res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
        }
    }, ()=>{
        res.send({success: 0, message: "El correo ingresado ya ha sido registrado previamente."})
    })
})

app.post("/cancelEmailVerificationCode", jsonParser, verifyAuthToken, (req, res) => {
    const {userID, email, newEmail} = req.body
    UserSettings.cancelEmailCode(userID, email, newEmail).then(() => {
        res.send({success: 1, message: "Se ha desactivado el token para la verificación de " + newEmail})
    }).catch(() => {
        res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
    })
})

app.put("/confirmChangeEmail", jsonParser, verifyAuthToken, (req, res) => {
    const {userID, newEmail, emailCode} = req.body
    Users.isNotRegistered(newEmail).then(async ()=>{
        if(await UserSettings.confirmEmailCode(userID, newEmail, emailCode)){
            const userUpdated = await Users.changeEmail(userID, newEmail)
            if(userUpdated){
                const {name, lastName, nickName, selectedPackage, fullAccount} = userUpdated
                const result = await Family.updateMyFamily(userID, name, lastName, nickName, newEmail, selectedPackage, fullAccount)
                if(result){
                    res.send({success: 1, message: "Información actualizada exitosamente"})
                } else {
                    res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
                }
            }
            else{
                res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
            }
        } else {
            res.send({success: 0, invalidToken: true, message: "El código de verificación no es correcto"})
        }
    }, ()=>{
        res.send({success: 0, message: "El correo ingresado ya ha sido registrado previamente."})
    })
})

/////////// Upgrade ///////////////////////

app.get("/verifyHalfDiscount", jsonParser, verifyAuthToken, async (req, res) =>{
    const { userID } = req.body
    const halfDiscount = await Tokens.verifyHalfDiscount(userID)
    res.send({success: 1, halfDiscount})
})

app.post("/upgradeAddress", jsonParser, verifyAuthToken, async (req, res) =>{
    let { userID, infoClient, newPackage } = req.body
    console.log("isers es ", Users, userID, infoClient)
    let payload = await Dispersion.createUpgradeTkn(userID, infoClient, newPackage)
    //console.log("llego al final", resp)
    res.send({success: 1, payload})
})

app.get("/upgrade", verifyAuthToken, async (req, res) =>{
        const { userID } = req.body
        let upgrade = await Dispersion.getUpgradeData(userID)
        res.send({success: 1, upgrade})
    })

app.post("/cancelUpgrade", jsonParser, verifyAuthToken, async (req, res) =>{
        const { upgradeID } = req.body
        await Dispersion.deleteUpgrade(upgradeID)
        res.send({success: 1})
    })

app.post('/confirmPaymentAndUpgrade', jsonParser, verifyAuthToken, async function (req, res) {
    let { userID } = req.body
    let upgrd = await Dispersion.getUpgradeData(userID)
    console.log("de activacion", upgrd)
    let {balance, mempool} = await getBalancePro(upgrd.address)
    console.log("de balance", balance)
    if(balance === 0 && mempool === 0){
        res.send({success: 0, message: "Aún no se ha confirmado su transacción, por favor intente más tarde."})
    } else if(balance === 0 && mempool > 0){
        res.send({success: 0, message: "Su transacción ha sido detectada pero aún no se encuentra confirmada, por favor intente más tarde."})
    } else {
        //si ya hay una cantidad en la billetera
        let quantityToFind = Constants.network === "mainnet" ? upgrd.quantity : 0.00022
        if(balance >= quantityToFind){
            try {
                console.log("si coincidio la cantidad, vamos a dispersar", balance, quantityToFind)
                let {txHex, txid} = await Dispersion.disperseCompany(balance, upgrd.address, upgrd.wif, false)
                console.log("se ejecuto la transaccion", txid)
                const userUpgraded = await Users.upgradeUser(userID, upgrd.newPackage, txid)
                if(userUpgraded){
                    const {email, name, lastName, nickName} = userUpgraded
                    const result = await Family.updateMyFamily(userID, name, lastName, nickName, email, upgrd.newPackage, true)
                    if(result){
                        res.send({success: 1, message: "¡Se ha mejorado tu cuenta!"})
                    } else {
                        res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
                    }
                }else{
                    res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
                } 
            } catch (error) {
                console.log("error en algo de dispers", error)
                res.send({success: 0, message: error.response.data})
            }
            
        } else {
            console.log("no coincidio la cantidad, vamos a dispersar", balance, quantityToFind)
            res.send({success: 0, message: "La cantidad enviada ha sido menor a la cantidad requerida para la activación."})
        }
    }
})

/////////// Get user info and update //////////

app.route("/user")
    .get(verifyAuthToken, async (req, res) => {
        const {userID} = req.body
        const user = await Users.getuser(userID)
        if(user)
            res.send({success: 1, user})
        else
            res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
    })
    .put(jsonParser, verifyAuthToken, async (req, res) => {
        const {userID, name, lastName, nickName, netPaymentsWallet} = req.body
        if(await Users.notExistNickName(userID, nickName)){
            const userUpdated = await Users.updateUserInfo(userID, name, lastName, nickName, netPaymentsWallet)
            if(userUpdated){
                const {email, selectedPackage, fullAccount} = userUpdated
                const result = await Family.updateMyFamily(userID, name, lastName, nickName, email, selectedPackage, fullAccount)
                if(result){
                    res.send({success: 1, message: "Información actualizada exitosamente"})
                } else {
                    res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
                }
            }
            else{
                res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
            }
        }else{
            res.send({success: 0, notAvailableNickName: true, message: "NickName ocupado, por favor escoge otro"})
        }
    })

/////////// Role Verify //////////

app.get("/amIdeveloper", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID} = req.body
    res.send({result: await Users.amIdeveloper(userID)})
})

app.get("/amItrader", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID} = req.body
    res.send({result: await Users.amItrader(userID)})
})

//////// Routes ProtextedBalances ///////

app.route("/protectedBalances")
    .get(verifyAuthToken, async (req, res) => {
        const {userID} = req.body
        if(await ProtectedBalances.verifyAccesToProtectedBalances(userID)){
            const balances = await ProtectedBalances.getProtectedBalances(userID)
            if(balances)
                res.send({success: 1, balances})
            else
                res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
        }else{
            res.send({success: 0, message: "Aun no tienes tus API's configuradas"})
        }
    })
    .post(jsonParser, verifyAuthToken, async (req, res) => {
        const {userID, balances} = req.body
        const result = await ProtectedBalances.updateProtectedBalances(userID, balances)
        if(result)
            res.send({success: 1, message: "Balances protegidos editados exitosamente"})
        else
            res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
    })

/////////// Get my payments //////////

app.get("/payments", verifyAuthToken, async (req, res) => {
    const {userID} = req.body
    let payments = await Payments.getMyPayments(userID)
    res.send({success: 1, payments})
})

/////////// Refferal payments //////////

app.get("/getactivations", verifyAuthToken, async (req, res) => {
    let activations = await Activations.getactivations()
    res.send({success: 1, activations})
})

app.post("/verifyDispersion", jsonParser, verifyAuthToken, async (req, res) => {
    const {idActiv, userID} = req.body
    const user = await Users.getuser(userID)
    const log = await Activations.verifyActiv(idActiv, user.selectedPackage)
    console.log("llevo el final")
    res.send({success: 1, log})
})

app.post("/sendDispersion", jsonParser, verifyAuthToken, async (req, res) => {
    const {idActiv, userID} = req.body
    const user = await Users.getuser(userID)
    const activation = await Activations.getOneActiv(idActiv)
    const { passed, error, dispersionData } = await Activations.preActivVerif(idActiv, user.selectedPackage)
    if(passed){
        const {company, transactions} = await Activations.getTransactions( dispersionData, user.selectedPackage )
        console.log("de las transacciones vino ", company, transactions)
        let {success, error, txid} = await sendCommissions(company, transactions, activation.wif, activation.address)
        if(success){
            res.send({success: 1, txid})
        } else 
            res.send({success: 0, message: error})
    } else {
        res.send({success: 0, message: error})
    }
})

///// STATS

app.get("/myOrdersHistory", verifyAuthToken, function (req, res) {
    let { userID } = req.body
    OrderKeepers.myOrdersHistory(userID).then(orders=>{
        res.send({success: 1, orders})
    })
})


app.get("/myOrdersActive", jsonParser, verifyAuthToken, async (req, res) => {
    const { userID } = req.body
    const orders = await OrderKeepers.myOrdersActive(userID)
    res.send({success: 1, orders})
})

// copy trade 
// deprecated
app.post("/editBuyOrders", jsonParser, verifyAuthToken, async (req, res) => {
    const {enterprice} = req.body
    OrderKeepers.editBuyOrders(enterprice).then(()=>{
        res.send({success: 1})
    })
})

// RecoverPassword

app.route("/recoverPassword")
    .post(jsonParser, async (req, res) => {
        const {email} = req.body
        if(email){
            const result = await RecoverPasswordTokens.generateToken(email);
            if(result.result){
                res.send({success: 1, message: "Se ha enviado un enlace a tu correo para recuperar tu contraseña"})
            }else{
                res.send({success: 0, message: result.message})
            }
        }else{
            res.send({success: 0, message: "Proporciona un email"})
        }
    })

app.route("/recoverPasswordBySms")
    .post(jsonParser, async (req, res) => {
        const {phone} = req.body
        if(phone){
            const result = await RecoverPasswordTokens.generateTokenSms(phone);
            if(result.result){
                res.send({success: 1, message: "Se ha enviado un enlace a tu teléfono para recuperar tu contraseña"})
            }else{
                res.send({success: 0, message: result.message})
            }
        }else{
            res.send({success: 0, message: "Proporciona un número de teléfono"})
        }
    })

app.route("/recoverPassword/:token")
    .get(jsonParser, async (req, res) => {
        const {token} = req.params
        if(token){
            const result = await RecoverPasswordTokens.verifyToken(token);
            if(result.result){
                res.send({success: 1})
            }else{
                res.send({success: 0, message: result.message})
            }
        }else{
            res.send({success: 0, message: "Proporciona un token"})
        }
    })
    .post(jsonParser, async (req, res) => {
        const {token} = req.params
        const {password} = req.body
        if(token && password){
            const result = await RecoverPasswordTokens.verifyToken(token)
            const {email, phone} = await RecoverPasswordTokens.desactivateToken(token)
            if(result.result){
                console.log(email, phone, password);
                if(email){
                    if(await Users.restorePassword(email, password)){
                        res.send({success: 1, message: "Se ha restablecido la contraseña"})
                    }else{
                        res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
                    }
                }else if(phone){
                    if(await Users.restorePasswordByPhone(phone, password)){
                        res.send({success: 1, message: "Se ha restablecido la contraseña"})
                    }else{
                        res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
                    }
                }else{
                    res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
                }
            }else{
                res.send({success: 0, message: result.message})
            }
        }else{
            res.send({success: 0, message: "Proporciona un token y una contraseña"})
        }
    })

app.post("/setTermsAndConditionsAgreement", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID} = req.body
    const result = await Users.setTermsAndConditionsAgreement(userID)
    if(result)
        res.send({success: 1, message: "Has aceptado los Terminos y condiciones"})
    else
        res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
})

///////// Bot ////////////

app.post("/activateBot", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID} = req.body
    const result = await Queues.activateBot(userID)
    if(result)
        res.send({success: 1, message: "Has activado el bot"})
    else
        res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
})

app.post("/desactivateBot", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID} = req.body
    const result = await Queues.desactivateBot(userID)
    if(result)
        res.send({success: 1, message: "Has desactivado el bot"})
    else
        res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
})

///////// 2FA ////////////////

app.get("/getTwoFactVerQRCode", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID} = req.body
    const result = await UserSettings.getTwoFactVerQRCode(userID)
    if(result)
        res.send({success: 1, qrcode: result})
    else
        res.send({success: 0, message: "Lo sentimos, ha ocurrido un error"})
})

app.post("/verifyTwoFactorTEMPSecret", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID, code} = req.body
    const result = await UserSettings.verifyTwoFactorTEMPSecret(userID, code)
    if(result)
        res.send({success: 1, message: "Verificado"})
    else
        res.send({success: 0, message: "Codigo invalido"})
})

app.post("/verifyTwoFactorSecret", jsonParser, (req, res) => {
    let {email, password, code} = req.body
    console.log(email, password, code);
    Users.login({email, password, active: true}).then(async (user)=>{
        if(!user.active){
            res.send({success: 0, message: "La cuenta ha sido registrada pero no está activa, por favor realice el pago de su licencia para activar la cuenta."})
        } else {
            const result = await UserSettings.verifyTwoFactorSecret(user._id.toString(), code)
            if(result){
                const userLegals = await Users.getTermsAndConditionsAgreement(user._id.toString())
                generateAuthToken(user._id.toString(), res)
                res.send({success: 1, user : {
                    id: user._id,
                    email: user.email,
                    selectedPackage: user.selectedPackage,
                    name: user.name,
                    trader: user.trader,
                    developer: user.developer,
                    fullAccount: user.fullAccount,
                    reservation: user.reservation,
                    admin: user.admin,
                    termsAndConditionsAgreement: userLegals
                }})
            }else{
                res.send({success: 0, message: "Codigo invalido"})
            }
        }
    }, ()=>{
        res.send({success: 0, message: "El usuario y/o contraseña son incorrectos"})
    })
})

app.get("/verifyTwoFactorActive", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID} = req.body
    const result = await UserSettings.verifyTwoFactorActive(userID)
    if(result)
        res.send({success: 1, result})
    else
        res.send({success: 0, result})
})

app.post("/desactivateTwoFactorAuth", jsonParser, verifyAuthToken, async (req, res) => {
    const {userID, code} = req.body
    const result = await UserSettings.desactivateTwoFactorAuth(userID, code)
    if(result)
        res.send({success: 1, result})
    else
        res.send({success: 0, message: "Codigo invalido"})
})

//funciones para arreglar a los usuarios
app.post("/fixUserInTree", jsonParser, verifyDeveloperKey, async (req, res) => {
    const {userID, newReffererID} = req.body
    const tkn = await Token.findOne({userID})
    console.log("el token es", tkn, tkn._id)
    //creamos el path con el id del nuevo referido 
    await Users.createPath(userID, newReffererID)
    await Token.findByIdAndUpdate(tkn._id, {
        payed: true,
        active: false,
        refererID: newReffererID
    })
    res.send({success: 1})
})

//funciones para mover referidos de un lado a otro
app.post("/moveUserReference", jsonParser, verifyDeveloperKey, async (req, res) => {
    const {userID, newReffererID} = req.body
    //traigo el objeto a mover
    const family = await Family.getTree(userID)

    console.log("la family y el path", family)
    let {success, message} = await Family.deleteFromTree(userID)
    if(success === 1){
        await Family.reconstructChilds([family], newReffererID, Users)
        res.send({success})
    } else {
        res.send({success, message})
    }
})

//funcion para activar un usuario manualmente fullAccount (quienes ya tenian pago)
app.post("/fixActivateAccount", jsonParser, verifyDeveloperKey, async (req, res) => {
    const {userID, txid} = req.body
    let activation = await Activations.getUserActivation(userID)
    await Payments.activationPayment(userID, "Activación de cuenta", activation.remainPayment, activation.quantity, activation.selectedPackage, activation.halfDiscount, txid, activation.priceBTCinEuros)
    console.log("se activo la cuenta")
    await Users.updateActivation(userID, {
        dispersedToCompany: 0,
        remainInWallet: 0,
        payed: true,
        active: false,
        txhash: txid,
        status: "Activacion Manual, preparar workspace"
    })
    await Users.findAndUpdate(userID, {
        fullAccount:true,
        reservation: false
    })
    const user = await Users.getuser(userID)
    await Family.updateMyFamily(user._id.toString(), user.name, user.lastName, user.nickName, user.email, user.selectedPackage, user.fullAccount)
    res.send({success: 1})
})

//funcion para activar un usuario manualmente fullAccount (skipping payment)
app.post("/activateAccount", jsonParser, verifyDeveloperKey, async (req, res) => {
    const {userID} = req.body
    const user = await Users.getuser(userID)
    await Users.findAndUpdate(userID, {
        fullAccount:true,
        reservation: false
    })
    
    await Family.updateMyFamily(user._id.toString(), user.name, user.lastName, user.nickName, user.email, user.selectedPackage, true)
    res.send({success: 1})
})

//para hacer cambios de paquetes
app.post("/setNewPackage", jsonParser, verifyDeveloperKey, async (req, res) => {
    const {userID, newPackage} = req.body
    try {
        const user = await Users.getuser(userID)
        await Users.findAndUpdate(userID, {
            selectedPackage: newPackage
        })
        
        await Family.updateMyFamily(userID, user.name, user.lastName, user.nickName, user.email, newPackage, user.fullAccount)
        res.send({success: 1})
    } catch (error) {
        
    }
    
})

//
app.get("/fixTreeDaemonPluss", verifyDeveloperKey, async (req, res) => {
    try {
        let allofTree = await Tree.find({})
        //console.log("todo el arbol es ", allofTree)
        await processPartOfTree(allofTree)
        res.send({success: 1})
    } catch (error) {
        res.send({success : 0, message : error.message ? error.message : error})
    }
})

app.get("/fixTreePaths", verifyDeveloperKey, async (req, res) => {
    try {
        let allUsers = await Users.getAll()
        //console.log("todo el arbol es ", allUsers)
        //await processPartOfTree(allofTree) 
        allUsers.map(async val=>{
            let {_id, name} = val 
            let path = await Path.find({userID: _id})
            //console.log("el path es", path)
            if(path.length === 0 ){
                console.log("el usuario no tiene path")
            } else {
                console.log("el usuario SI tiene path")
            }
        })
        res.send({success: 1})
    } catch (error) {
        res.send({success : 0, message : error.message ? error.message : error})
    }
})

app.get("/fixTree", verifyDeveloperKey, async (req, res) => {
    try {
        
        let allUsers = await getData()
        console.log("todos los usuarios son", allUsers)
        let treeNodes = []
        let notEncountered = []
        let promises = []
        allUsers.map(val=>promises.push(processFixUser(val, treeNodes, notEncountered)))

        Promise.all(promises).then(()=>{
            console.log("al final el arbol queda", treeNodes, "los no encontered", notEncountered)
            saveData(treeNodes).then(()=>{
                res.send({success: 1})
            })
        })
        
    } catch (error) {
        res.send({success : 0, message : error.message ? error.message : error})
    }
})

function saveData(treeNodes){
    return new Promise((resolve, reject)=>{
        treeNodes.map(async val=>{
            let nodeTree = new Tree(val)
            await nodeTree.save()
        })
        resolve()
    })
    
}

async function getData(){
    let allUsers = await Users.getAll()
    //console.log("todo el arbol es ", allUsers)
    //await processPartOfTree(allofTree) 
    let allCompletedUsers = []
    do {
        let usr = await allUsers.shift()
        let path = await Path.findOne({userID: usr._id})
        let token = await Token.findOne({userID: usr._id, $or : [{active: true}, {payed: true}, {txhash: {$exists: true}}] })
        if(token){
            if(path && path.path){
                //console.log("voy a mezclar", usr, path, token)
                let {_id, name, lastName, nickName, email, selectedPackage, createdAt, fullAccount} = usr
                allCompletedUsers.push({
                    path: path.path,
                    halfDiscount: token.halfDiscount,
                    refererID: token.refererID,
                    _id, name, lastName, nickName, email, selectedPackage, createdAt, fullAccount
                })
            } else {
                console.log("no tiene path este usuario", usr.name, usr._id)
            }
        } else {
            console.log("no tiene token este usuario", usr.name, usr._id)
        } 
    } while (allUsers.length > 0);
    allCompletedUsers.sort(function(a, b){
        return a.path.length - b.path.length;
    });
    return allCompletedUsers
}

function processFixUser(val, treeNodes, notEncountered){
    return new Promise(async (resolve, reject)=>{
        let {_id, name, lastName, nickName, email, selectedPackage, createdAt, fullAccount, halfDiscount, refererID, path} = val
        let newUser = {
            email, name, selectedPackage, userID : _id, createdAt, halfDiscount, lastName, nickName, fullAccount, childs : []
        }
        if(path.length > 0){
            await searchAndInsert(treeNodes, path, newUser, refererID, notEncountered)
            resolve()
        } else {
            treeNodes.push(newUser)
            resolve()
        }
    })
}

function searchAndInsert(treeNodes, path, newUser, refererID, notEncountered){
    return new Promise(async (resolve, reject)=>{
        let current =  path.shift()
        //console.log("el current path es", current)
        let position = false
        treeNodes.map((val, index)=>{
            //console.log(" paso x", val.userID, current)
            if(val.userID == current){
                //console.log("si paso", val.userID, current)
                position = index
            }
        })
        //console.log("el tree", treeNodes, position, )
        if(treeNodes[position] && treeNodes[position].childs){
            if(path.length > 0){
                await searchAndInsert(treeNodes[position].childs, path, newUser, refererID, notEncountered)
                resolve()
            } else {
                treeNodes[position].childs.push(newUser)
                resolve()
            }
        } else {
            console.log("no se pudo registrar", newUser.name, refererID)
            if(!notEncountered.includes(refererID)){
                let pathRef = await Path.findOne({userID: refererID})
                console.log("el path del que no se le registro es ", pathRef, newUser.name)
                notEncountered.push(refererID)
                resolve()
            } else {
                resolve()
            }
        }
    })
    
    
}

async function processPartOfTree(childs) {
    childs.map(async val=>{
        //console.log("pasando", val)
        let { userID,  fullAccount, reservation, halfDiscount, name, childs} = val
        console.log("buscare en la persona ", name)
        // let user = await Users.getByID(userID)
        // let path = await Path.findOne({userID})
        // if(!path){
        //     console.log("no tiene un path", name)
        // } else {
        //     console.log("si tiene un path", path)
        // }
        //console.log("el usuario es ", user)
        //if(user && fullAccount != user.fullAccount){
            //console.log("Existe una diferencia en el fullacount", fullAccount,  user.fullAccount, user.name)
        //}
        if(childs && childs.length > 0 ){
            
            await processPartOfTree(childs)
        }
    })
}

module.exports = app