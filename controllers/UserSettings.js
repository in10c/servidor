const UserSettingsModel = require("../models/UserSettings");
const {sendVerificationMail} = require("../services/mailer");
const {generateSecretAndQR, verifySecret} = require("../services/googleAuth");

class UserSettings {
    async saveEmailCode(userID, email, newEmail, emailCode){  
        try {
            await sendVerificationMail(newEmail, emailCode)
            console.log("se envio el correo")
            const settings = await UserSettingsModel.find({userID})
            if(settings.length === 0){
                const newSetting = new UserSettingsModel({
                    userID,
                    email,
                    newEmail,
                    emailCode,
                    confirmedEmail: false,
                    inVerificationEmailProcess: true
                })
                await newSetting.save()
            }else{
                await UserSettingsModel.findOneAndUpdate({userID}, {
                    newEmail,
                    emailCode,
                    confirmedEmail: false,
                    inVerificationEmailProcess: true
                })
            }
            return true
        } catch (error) {
            console.log("error email confirm", error)
            return false
        }
    }
    cancelEmailCode(userID, email, newEmail){  
        return new Promise((resolve, reject)=>{
            UserSettingsModel.findOneAndUpdate({userID, email, newEmail, inVerificationEmailProcess: true}, {inVerificationEmailProcess: false}).then(() => {
                resolve()
            })
        }, err=>{
            console.log("error cancel email confirm", err)
            reject()
        });
    }
    async confirmEmailCode(userID, newEmail, emailCode){  
        try {
            const tokens = await UserSettingsModel.find({userID, newEmail, emailCode, inVerificationEmailProcess: true})
            console.log(tokens)
            if(tokens.length === 0){
                return false
            }
            await UserSettingsModel.findOneAndUpdate(
                {userID, newEmail, emailCode, inVerificationEmailProcess: true}, 
                {inVerificationEmailProcess: false, confirmedEmail: true, email: newEmail}
            )
            return true
        } catch (error) {
            console.log("error confirm email token " + error)
            return false
        }
    }
    async getTwoFactVerQRCode(userID){
        try {
            const {secret, key, qrCodeBase64PNG} = await generateSecretAndQR()
            const settings = await UserSettingsModel.findOne({userID})
            if(!settings){
                const newSetting = new UserSettingsModel({
                    userID,
                    twoFactorTempSecret: secret,
                    qrCode: qrCodeBase64PNG,
                    key
                })
                await newSetting.save()
            }else if(settings.twoFactorTempSecret && settings.qrCode && settings.key){
                return {key: settings.key, qrcode: settings.qrCode}
            }else{
                await UserSettingsModel.findOneAndUpdate({userID}, {
                    twoFactorTempSecret: secret,
                    qrCode: qrCodeBase64PNG,
                    key
                })
            }
            return {key, qrcode: qrCodeBase64PNG}
        } catch (error) {
            console.log("error at getTwoFactVerQRCode", error)
            return false
        }
    }
    async verifyTwoFactorTEMPSecret(userID, code){
        try {
            const settings = await UserSettingsModel.findOne({userID})
            if(!settings){
                return false
            }
            if(!(settings.twoFactorTempSecret && settings.qrCode)){
                return false
            }
            const isVerified = verifySecret(settings.twoFactorTempSecret, code)
            console.log(isVerified);
            if(isVerified){
                await UserSettingsModel.findOneAndUpdate({userID},{
                    twoFactorTempSecret: null,
                    qrCode: null,
                    key: null,
                    twoFactorEnabled: true,
                    twoFactorSecret: settings.twoFactorTempSecret
                })
            }
            return isVerified
        } catch (error) {
            console.log("error at verifyTwoFactorTEMPSecret", error)
            return false
        }
    }
    async verifyTwoFactorSecret(userID, code){
        try {
            const settings = await UserSettingsModel.findOne({userID})
            if(!settings){
                return false
            }
            if(!(settings.twoFactorEnabled && settings.twoFactorSecret)){
                return false
            }
            const isVerified = verifySecret(settings.twoFactorSecret, code)
            console.log(isVerified);
            return isVerified
        } catch (error) {
            console.log("error at verifyTwoFactorSecret", error)
            return false
        }
    }
    async verifyTwoFactorActive(userID){
        try {
            const settings = await UserSettingsModel.findOne({userID})
            if(!settings){
                return false
            }
            return !!settings.twoFactorEnabled
        } catch (error) {
            console.log("error at verifyTwoFactorActive", error)
            return false
        }
    }
    async desactivateTwoFactorAuth(userID, code){
        try {
            const settings = await UserSettingsModel.findOne({userID})
            if(!settings){
                return false
            }
            if(!(settings.twoFactorEnabled && settings.twoFactorSecret)){
                return false
            }
            const isVerified = verifySecret(settings.twoFactorSecret, code)
            console.log(isVerified);
            if(isVerified){
                await UserSettingsModel.findOneAndUpdate({userID},{
                    twoFactorTempSecret: null,
                    qrCode: null,
                    key: null,
                    twoFactorEnabled: null,
                    twoFactorSecret: null
                })
            }
            return isVerified
        } catch (error) {
            console.log("error at desactivateTwoFactorAuth", error)
            return false
        }
    }
}

module.exports = new UserSettings()