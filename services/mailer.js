var nodemailer = require("nodemailer")
const Constant = require("../services/constants")
const packs = {
    1: "Bronce",
    2: "Plata",
    3: "Oro",
    4: "Diamante",
    5: "Premium",
    6: "Premium PLUS"
}

const sendMail = (email, nombre, selectedPackage, txid) => {
    return new Promise((resolve, reject)=>{
        let transporter = nodemailer.createTransport({
            host: "smtp.ionos.es",
            port: 465,
            secure: true, 
            auth: {
                user: "info@bitwabi.com", 
                pass: "@Bitwa69", 
            },
        });
    
        transporter.sendMail({
            from: '"IT Bitwabi" <info@bitwabi.com>', 
            to: Constant.production ? email : "criztian.torr3s@gmail.com", 
            subject: "Bienvenido a Bitwabi", 
            html: `¡Bienvenido a Bitwabi `+nombre+`!, <br/><br/>Le enviamos este correo para confirmarle que el pago de su licencia 
            `+packs[selectedPackage]+` ha sido recibido con éxito y su cuenta ya está activa.<br/><br/>
            Su ID de transacción es el siguiente: <a href="`+Constant.btcExplorer()+`/`+txid+`" target="_blank">`+txid+`</a><br/><br/>
            Para acceder a su cuenta, entre a <a href="`+Constant.appUrl()+`" target="_blank">app.bitwabi.com</a> y acceda mediante las credenciales registradas.<br/><br/>
            Atentamente, el equipo de Bitwabi.
            `, 
        }).then(info=>{
            resolve()
        })
    })
    
}

const sendActivationMail = (email, nombre, selectedPackage, txid) => {
    return new Promise((resolve, reject)=>{
        let transporter = nodemailer.createTransport({
            host: "smtp.ionos.es",
            port: 465,
            secure: true, 
            auth: {
                user: "info@bitwabi.com", 
                pass: "@Bitwa69", 
            },
        });
    
        transporter.sendMail({
            from: '"IT Bitwabi" <info@bitwabi.com>', 
            to: Constant.production ? email : "criztian.torr3s@gmail.com", 
            subject: "Licencia activada", 
            html: `¡`+nombre+`, su licencia ha sido activada!, <br/><br/>Le enviamos este correo para confirmarle que el pago restante de su licencia 
            `+packs[selectedPackage]+` ha sido recibido con éxito y su cuenta ya está completamente activa.<br/><br/>
            Su ID de transacción es el siguiente: <a href="`+Constant.btcExplorer()+`/`+txid+`" target="_blank">`+txid+`</a><br/><br/>
            Para acceder a su cuenta, entre a <a href="`+Constant.appUrl()+`" target="_blank">app.bitwabi.com</a> y acceda mediante las credenciales registradas.<br/><br/>
            Atentamente, el equipo de Bitwabi.
            `, 
        }).then(info=>{
            resolve()
        })
    })
    
}

const sendVerificationMail = (email, emailCode) => {
    return new Promise((resolve, reject)=>{
        let transporter = nodemailer.createTransport({
            host: "smtp.ionos.es",
            port: 465,
            secure: true, 
            auth: {
                user: "info@bitwabi.com", 
                pass: "@Bitwa69", 
            },
        });
    
        transporter.sendMail({
            from: '"IT Bitwabi" <info@bitwabi.com>', 
            to: email, 
            subject: "Código de verificación Bitwabi", 
            html: `Su código de verificación de correo electrónico de Bitwabi es: <b>${emailCode}</b> . 
            <br/>
            Si usted no ha solicitado el cambio de correo electrónico de su cuenta Bitwabi solo ignore el contenido de este correo, gracias.
            <br/>
            <br/>
            Atentamente, el equipo de Bitwabi.
            `, 
        }).then(info=>{
            resolve()
        }).catch(error => {
            reject(error)
        })
    })
    
}

const sendUpgradeMail = (email, nombre, oldSelectedPackage, selectedPackage, txid) => {
    return new Promise((resolve, reject)=>{
        let transporter = nodemailer.createTransport({
            host: "smtp.ionos.es",
            port: 465,
            secure: true, 
            auth: {
                user: "info@bitwabi.com", 
                pass: "@Bitwa69", 
            },
        });
    
        transporter.sendMail({
            from: '"IT Bitwabi" <info@bitwabi.com>', 
            to: Constant.production ? email : "criztian.torr3s@gmail.com", 
            subject: "Mejoraste tu cuenta", 
            html: `¡Felicidades `+nombre+`!, <br/><br/>Nos complace informarle que ha mejorado su licencia de ${packs[oldSelectedPackage]} a ${packs[selectedPackage]}.
            <br/><br/>Le enviamos este correo para confirmarle que el pago de su nueva licencia
            `+packs[selectedPackage]+` ha sido recibido con éxito, muchas gracias.<br/><br/>
            Su ID de transacción es el siguiente: <a href="`+Constant.btcExplorer()+`/`+txid+`" target="_blank">`+txid+`</a><br/><br/>
            Para acceder a su cuenta, entre a <a href="`+Constant.appUrl()+`" target="_blank">app.bitwabi.com</a>.<br/><br/>
            Atentamente, el equipo de Bitwabi.
            `, 
        }).then(info=>{
            resolve()
        }).catch(error => {
            reject(error)
        })
    })
    
}

const sendRecoverPasswordEmail = (email, link) => {
    return new Promise((resolve, reject)=>{
        let transporter = nodemailer.createTransport({
            host: "smtp.ionos.es",
            port: 465,
            secure: true, 
            auth: {
                user: "info@bitwabi.com", 
                pass: "@Bitwa69", 
            },
        });
    
        transporter.sendMail({
            from: '"IT Bitwabi" <info@bitwabi.com>', 
            to: Constant.production ? email : "criztian.torr3s@gmail.com", 
            subject: "Recuperación de contraseña", 
            html: `Ha solicitado cambiar su contraseña.
            <br/><br/>Mediante <a href="${link}" target="_blank">éste enlace</a> puede cambiarla.
            <br/><br/>Si el enlace anterior no le funciona, copie y pegue esta dirección de su navegador de internet.
            <br/><br/>${link}<br/><br/>
            Atentamente, el equipo de Bitwabi.
            `, 
        }).then(info=>{
            resolve()
        }).catch(error => {
            reject(error)
        })
    })
    
}

const sendActivatePaymentMail = (email, address, remainPaymentBTC, link) => {
    remainPaymentBTC = (parseFloat(remainPaymentBTC)+0.0004).toFixed(5)
    return new Promise((resolve, reject)=>{
        let transporter = nodemailer.createTransport({
            host: "smtp.ionos.es",
            port: 465,
            secure: true, 
            auth: {
                user: "info@bitwabi.com", 
                pass: "@Bitwa69", 
            },
        });
    
        transporter.sendMail({
            from: '"IT Bitwabi" <info@bitwabi.com>', 
            to: Constant.production ? email : "criztian.torr3s@gmail.com", 
            subject: "Dirección de pago para activación de licencia Bitwabi", 
            html: `Su dirección de envío ha sido generada. <br/><br/>
            Por favor envie la cantidad de <b>${remainPaymentBTC} BTC</b> a la dirección:<br/><br/>
            <b>${address}</b><br/><br/>
            Después de que haya sido confirmada su transacción, ingrese a <a href="${link}" target="_blank">esté enlace</a> para continuar con la activación de su cuenta.<br/><br/>
            En caso de no funcionar el enlace anterior, copie y pegue la siguiente dirección url en su navegador:<br/><br/>
            ${link}<br/><br/>
            Atentamente, el equipo de Bitwabi.
            `, 
        }).then(info=>{
            resolve()
        })
    })
    
}

exports.sendMail = sendMail
exports.sendActivationMail = sendActivationMail
exports.sendVerificationMail = sendVerificationMail
exports.sendUpgradeMail = sendUpgradeMail
exports.sendRecoverPasswordEmail = sendRecoverPasswordEmail
exports.sendActivatePaymentMail = sendActivatePaymentMail