const RecoverPasswordToken = require("../models/RecoverPasswordToken");
const Tokens = require("./Tokens");
const Users = require("./Users");
const { sendRecoverPasswordEmail } = require("../services/mailer");
const Constants = require("../services/constants");

const accountSid = true ? "AC424ef72df1be3a7f774a85a5a199bb60" : 'AC8379e5d71497f4ea1aa4c30103f50ed0';
const authToken = true ? 'd2d37bb70b7c4b7a48b4a7ccd2f2d793' : "68054670f1804929922cd6753460a7a1";
const clientSMS = require('twilio')(accountSid, authToken);

class RecoverPasswordTokens {
    async generateToken(email) {
        try {
            const now = new Date(Date.now());
            const activeTokens = await RecoverPasswordToken.findOne({
                email,
                active: true,
                expiresIn: {
                    $gte: now,
                },
            });
            if (activeTokens) {
                const expires = new Date(activeTokens.expiresIn);
                return { result: false, message: "Ya hemos enviado un token a tu correo" };
            }
            if (!(await Users.existsEmail(email))) {
                return { result: false, message: "Lo sentimos, no hemos podido encontrar tu correo" };
            }
            const token = Tokens.generateEmailCode();
            const expiresIn = new Date(Date.now());
            expiresIn.setSeconds(60 * 15);
            await RecoverPasswordToken.create({
                email,
                token,
                active: true,
                expiresIn,
            });
            await sendRecoverPasswordEmail(email, Constants.appUrl() + "/recoverPassword/" + token);
            return { result: true };
        } catch (error) {
            console.log("error at generate recover token", error.message);
            return { result: false, message: "Lo sentimos, ha ocurrido un error" };
        }
    }

    async generateTokenSms(phone) {
        try {
            const now = new Date(Date.now());
            const activeTokens = await RecoverPasswordToken.findOne({
                phone,
                active: true,
                expiresIn: {
                    $gte: now,
                },
            });
            if (activeTokens) {
                const expires = new Date(activeTokens.expiresIn);
                return { result: false, message: "Ya hemos enviado un token a tu teléfono" };
            }
            if (!(await Users.existsPhone(phone))) {
                return { result: false, message: "Lo sentimos, no hemos podido encontrar tu número de teléfono" };
            }
            const token = Tokens.generateSMSCode();
            const expiresIn = new Date(Date.now());
            expiresIn.setSeconds(60 * 15);
            await RecoverPasswordToken.create({
                phone,
                token,
                active: true,
                expiresIn,
            });
            await clientSMS.messages.create({
                body: 'Ha solicitado cambiar su contraseña. Mediante éste enlace puede cambiarla: '
                + Constants.appUrl() + "/recoverPassword/" + token + 
                ' Atentamente, el equipo de Bitwabi.',
                from: '+19705172699',
                to: Constants.production ? "+"+phone : "+525543671245"
            });
            return { result: true };
        } catch (error) {
            console.log("error at generate recover token sms", error.message);
            return { result: false, message: "Lo sentimos, ha ocurrido un error" };
        }
    }

    async verifyToken(recoverPasswordToken) {
        try {
            console.log(recoverPasswordToken);
            const now = new Date(Date.now());
            const token = await RecoverPasswordToken.findOne({ 
                token: recoverPasswordToken,
                active: true,
                expiresIn: {
                    $gte: now,
                }, });
            if (!token) {
                return { result: false, message: "Este token no existe. ha expirado o ya ha sido usado" };
            }
            const expiresIn = new Date(token.expiresIn);
            if (now > expiresIn) {
                return { result: false, message: "Este token ya ha expirado" };
            }
            return { result: true };
        } catch (error) {
            console.log("error at verify recover token", error.message);
            return { result: false };
        }
    }

    async desactivateToken(token) {
        try {
            const tkn = await RecoverPasswordToken.findOneAndUpdate({ token, active: true }, { active: false });
            return { email: tkn.email, phone: tkn.phone };
        } catch (error) {
            console.log("error at desactivate recover token", error.message);
            return false;
        }
    }
}

module.exports = new RecoverPasswordTokens();
