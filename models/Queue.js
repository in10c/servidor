var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var QueueSchema = Schema({
    userID: String,
    benefitsPercent: Number
});

module.exports = mongoose.model('Queue', QueueSchema);