var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var ActivationSchema = Schema({
    userID: String,
    address: String,
    quantity: Number,
    selectedPackage: Number,
    generatedTime: Date,
    //only for the 50 discount
    halfDiscount: Boolean,
    priceBTCinEuros: Number,
    payed: Boolean,
    active: Boolean,
    remainPayment : Number,
    dispersedToCompany: Number,
    remainInWallet: Number,
    wif: String,
    pendingDispersion: Array,
    txhash: String,
    infoClient : {
        ip: String,
        country_name: String,
        browser: String,
        device: String,
        os: String
    },
    status: String
}, {timestamps: true});

module.exports = mongoose.model('Activation', ActivationSchema);