const Binance = require('binance-api-node').default
var mongoose = require('mongoose');
const Constants = require("../services/constants")
const Api = require("../models/Api")
const Order = require("../models/Order")
const OrdersQueue = require("../models/OrdersQueue")
const {fixedToPattern} = require("../services/numbers")

//global variables
const {userID, inPair, outPair, sl, enterprice, gain, type, signalID, processID, lotSize} = JSON.parse(process.argv[2])
const Symbol = inPair+""+outPair
var ocoOrderID = false
var orden_id, odenqueue_id, socketBinance, binanceClient, socketParent; 

console.log("inicie order keep con ", userID, inPair, outPair, sl, enterprice, gain)

const connectDatabase = (cb) => {
    mongoose.Promise = global.Promise;
    // Usamos el método connect para conectarnos a nuestra base de datos
    mongoose.connect(Constants.databaseStringConn(), { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
    .then(() => {
        cb()
    })
    .catch(err => console.log(err));
}
//deprecated
const createOCO = async (_sl, _gain) =>{
    try {
        //si existe una oco, la eliminamos
        if(ocoOrderID){
            await binanceClient.cancelOrder({
                symbol: Symbol,
                origClientOrderId: ocoOrderID.orders[0].clientOrderId,
            })
        }
        //traemos nuevo balance
        let quantityToSell = await getCoinBalance(inPair)
        quantityToSell = fixedToPattern(quantityToSell, lotSize)
        //asignamos balance a orden
        let order = {
            symbol: Symbol, side: type === "SELL" ? "BUY" : "SELL",
            quantity: quantityToSell, price: _gain, stopPrice: _sl, stopLimitPrice: _sl,
        }
        //creamos nueva OCO
        let respOCO = await binanceClient.orderOco(order)
        //console.log("la respuesta de crearoco es ", respOCO)
        ocoOrderID = respOCO
        return {success: true}
    } catch (error) {
        console.log("ocurrio error en crear oco :( ", error)
        return {success: false, message: error.message}
    }
}
//deprecated
const createBuyOrder = async (balance, _enterprice) =>{
    await binanceClient.order({
        symbol: Symbol,
        side: type,
        quantity: balance,
        price: _enterprice ? _enterprice : enterprice,
    })
}
//deprecated
const updateOrder = async (updtObj) =>{
    await Order.findByIdAndUpdate( orden_id, updtObj)
    await OrdersQueue.findByIdAndUpdate(odenqueue_id, updtObj)
}

const deleteThis = async (sock_buyOrder) => {
    if(sock_buyOrder){
        let resp = await binanceClient.cancelOrder({
            symbol: Symbol,
            origClientOrderId: sock_buyOrder,
        })
        console.log("respuesta de cancelar orden compra", resp)
    }
    if(ocoOrderID){
        console.log("el oco order", ocoOrderID)
        let resp = await binanceClient.cancelOrder({
            symbol: Symbol,
            origClientOrderId: ocoOrderID.orders[0].clientOrderId,
        })
        console.log("respuesta de cancelar orden", resp)
        let quantityToSell = await getCoinBalance(inPair)
        quantityToSell = fixedToPattern(quantityToSell, lotSize)
        let result = await binanceClient.order({
            type: "MARKET", symbol: Symbol, side: "SELL", quantity: quantityToSell,
        })
        console.log("respuesta de crear market", result)
    }
    process.exit(1)
}

const createInstanceAndGetBalance = async() => {
    try {
        let res = await Api.findOne({userID})
        binanceClient = Binance({
            apiKey: res.apiKey,
            apiSecret: res.apiSecret
        })
        setSocket()
        let balance = await getBuyBalance()
        return balance
    } catch (error) {
        console.log("error al traer api", error)
        return false
    }
}

const setSocket = () => {
    let sock_buyOrder = null
    let totalBuyed = 0
    let totalSelled = 0
    //socket de binance
    socketBinance = binanceClient.ws.user(async msg => {
        
        console.log(">>>>>>>>> actualizacion del usuario ",msg)
        
        if(msg.eventType === "executionReport" && msg.symbol === Symbol && msg.side === "BUY"){
            //si una orden es nueva
            if(msg.orderStatus === "NEW"){
                sock_buyOrder = msg.newClientOrderId
                updateOrder({
                    globalStatus: "Orden de compra creada",
                    buyOrderID: msg.newClientOrderId,
                    buyedQuantity: msg.totalTradeQuantity,
                    buyOrderQuantity: msg.quantity,
                    buyOrderStatus: msg.orderStatus,
                    buyOrderExecutionTime: msg.creationTime,
                    buyOrderPrice: msg.price,
                })
            //si una orden de compra ya se lleno o parcial
            } else if(msg.orderStatus === "PARTIALLY_FILLED" || msg.orderStatus === "FILLED"){
                sock_buyOrder = null
                let balanceToSell = msg.totalTradeQuantity
                //si la comision fue tomada del mismo par
                if(msg.commissionAsset === inPair){
                    console.log("*** le quito la comision")
                    balanceToSell -= parseFloat(msg.commission)
                }
                balanceInUse = fixedToPattern(balanceToSell, lotSize)
                let {success, message} = await createOCO(sl, gain)
                if(success){
                    let partial = msg.orderStatus === "PARTIALLY_FILLED" 
                    //para la resta final
                    totalBuyed += parseFloat(msg.lastQuoteTransacted)
                    await updateOrder({ buyOrderStatus: msg.orderStatus, globalStatus: "Orden de compra "+(partial ? "parcialmente" : "" )+" ejecutada", 
                        buyedQuantity: msg.totalTradeQuantity, spendedQuantity: totalBuyed })
                } else {
                    //para la resta final
                    totalBuyed += parseFloat(msg.lastQuoteTransacted)
                    await updateOrder({ buyOrderStatus: msg.orderStatus, globalStatus: "Fallo al crear la orden OCO por lo siguiente: "+message, 
                        buyedQuantity: msg.totalTradeQuantity, spendedQuantity: totalBuyed })
                }
                
                
            //para los otros estados
            } else if (msg.orderStatus === "CANCELED") {
                sock_buyOrder = null
                await updateOrder({ buyOrderStatus: msg.orderStatus, globalStatus: "La orden de compra ha sido cancelada"})
            } else {
                sock_buyOrder = null
                await updateOrder({ buyOrderStatus: msg.orderStatus, globalStatus: "La orden de compra tiene un estado extraño" })
            }
        } else if (msg.eventType === "executionReport" && msg.symbol === Symbol && msg.side === "SELL") {
            
            if(msg.orderStatus === "NEW" && msg.orderType === "LIMIT_MAKER"){
                updateOrder({
                    globalStatus: "Orden de venta creada",
                    sellOrderID: msg.newClientOrderId,
                    sellOrderQuantity: msg.quantity,
                    sellOrderPrice: msg.price,
                    sellOrderExecution: msg.creationTime,
                    sellOrderStatus: msg.orderStatus
                })
            //si una orden de compra ya se lleno o parcial
            } else if(msg.orderStatus === "PARTIALLY_FILLED" || msg.orderStatus === "FILLED"){
                totalSelled += parseFloat(msg.lastQuoteTransacted)
                let diff = totalSelled - totalBuyed
                let percent = diff * 100 / totalBuyed
                let partial = msg.orderStatus === "PARTIALLY_FILLED" 
                await updateOrder({ 
                    sellOrderStatus: msg.orderStatus,
                    globalStatus: "Orden de venta "+(partial ? "parcialmente" : "" )+" ejecutada", 
                    selledQuantity: msg.totalTradeQuantity, 
                    receivedQuantity: totalSelled, 
                    sellOrderType: msg.orderType,
                    resultantQuantity: diff,
                    resultantPercentage: percent
                })
                if(msg.orderStatus === "FILLED"){
                    process.exit(1)
                }
            //para los otros estados
            } else if (msg.orderStatus === "CANCELED" && msg.orderType === "LIMIT_MAKER") {
                await updateOrder({ sellOrderStatus: msg.orderStatus, globalStatus: "La orden de venta ha sido cancelada"})
            } 
        }
    })
    //socket para ordenes desde fuera
    socketParent = process.on("message", payload=>{
        console.log("llego un mensaje al hijo", payload)
        switch (payload.type) {
            case "delete":
                deleteThis(sock_buyOrder)
                break;
            case "editoco":
                createOCO(payload.sl, payload.tp)
                break;
            case "editbuy":
                editBuyOrder(payload.enterprice, sock_buyOrder)
                break;
            default:
                break;
        }
    })
}

const editBuyOrder = async (enterprice, sock_buyOrder) =>{
    if(sock_buyOrder){
        console.log("soi se eliminara", sock_buyOrder)
        await binanceClient.cancelOrder({
            symbol: Symbol,
            origClientOrderId: sock_buyOrder,
        })
        let buyBalance = await getBuyBalance()
        await createBuyOrder(buyBalance, enterprice)
    } else {
        console.log("no se eliminara la orden de compra porque no hay order id")
    }
}
//deprecated
const getBuyBalance = async () =>  {
    let balance = await getCoinBalance("BTC")
    let stats = await binanceClient.dailyStats({ symbol: Symbol })
    //si es compra se divide por el precio de compra
    let balancePortion = balance * Constants.enterPercentage
    let quantityToBuy = fixedToPattern(balancePortion / stats.askPrice, lotSize)
    return quantityToBuy
}
//deprecated
const getCoinBalance = async (coin) => {
    let balances = await binanceClient.accountInfo({'recvWindow': 50000})
    let filt = balances.balances.filter(val=> val.asset === coin)
    //console.log("tragio coin balance ", coin, balances.balances, filt[0], filt[0].free)
    return filt[0].free
}
//deprecated
//generamos el registro en la base de datos
const generateRegistry = async () => {
    let today = new Date()
    let order = { userID, symbol: Symbol, processID, sl, enterprice, gain, side: type, 
        signalID, globalStatus: "Proceso generado", transactTime: today.getTime()
    }
    let respOrd = await new Order(order).save()
    let respOrdQu = await new OrdersQueue(order).save()
    orden_id = respOrd._id
    odenqueue_id = respOrdQu._id
}
//deprecated
//iniciamos
connectDatabase(async ()=>{
    await generateRegistry()
    let quantityToBuy = await createInstanceAndGetBalance()
    //si no se pudo traer la cantidad a comrpar se guarda y se cierra el proceso
    if(!quantityToBuy) {
        console.log("no se trajo cantidad a comprar, se cerrara")
        await updateOrder({ globalStatus: "No se pudo traer una cantidad a comprar" })
        process.exit(1)
    } else {
        console.log("si se trajo cantidad", quantityToBuy)
        setTimeout(()=>{
            createBuyOrder(quantityToBuy)
        }, 1000)
        
    }    
})