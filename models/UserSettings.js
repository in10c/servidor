var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var UserSettingsSchema = Schema({
    userID: String,
    email: String,
    newEmail: String,
    emailCode: String,
    confirmedEmail: Boolean,
    inVerificationEmailProcess: Boolean,
    twoFactorTempSecret: String,
    qrCode: String,
    key: String,
    twoFactorEnabled: Boolean,
    twoFactorSecret: String
}, {timestamps: true});

module.exports = mongoose.model('UserSettings', UserSettingsSchema);