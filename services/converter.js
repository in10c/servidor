const bitcoin = require('bitcoinjs-lib')

class ConverterClass {
    convert(string){
        /*
            bitcoinjs.networks.litecoin = { 
            messagePrefix: '\u0019Litecoin Signed Message:\n',
            bech32: 'ltc',
            bip32: { public: 76067358, private: 76066276 },
            pubKeyHash: 48,
            scriptHash: 50,
            wif: 176 }
        */
        let address = bitcoin.address.fromBech32(string)
        let base58 = bitcoin.address.toBase58Check(address.data, 48)
        return base58
    }
}
module.exports = new ConverterClass();